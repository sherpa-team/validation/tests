from os import path
from subprocess import check_output

class sherpa_installinfo(object):

    def __init__(self, prefix):

        self.prefix     = prefix
        self.bin        = path.join(prefix, 'bin/Sherpa')
        self.config_bin = path.join(prefix, 'bin/Sherpa-config')
        self.generate_model_bin = path.join(prefix, 'bin/Sherpa-generate-model')

    def get_bin(self):
        return self.bin

    def get_generate_model_bin(self):
        return self.generate_model_bin

    def get_loadmpi4py(self):
        return check_output([self.config_bin,  '--load-mpi4py']).strip()

    def get_pylibdir(self):
        return check_output([self.config_bin,  '--python-libs']).strip()
