from os import path
import imp

class test_model(object):

    all_test_models = list()

    def __init__(self, ufo_name, ufo_path, param_card_path):
        self._ufo_path   = ufo_path
        self._name       = ufo_name
        self._param_card = param_card_path
        try:
            f,pathname, desc = imp.find_module(self._name, [ufo_path])
            self._ufo_model  = imp.load_module(self._name, f, pathname, desc)
        except ImportError:
            raise ValueError("Could not import model {0} from directory {1}".format(ufo_name, ufo_path))

        self.all_test_models.append(self)

    def __str__(self):
        return self._name

    def name(self):
        return self._name

sm        = test_model('sm',        path.abspath('./'), 'param_card_sm.dat')
heft      = test_model('heft',      path.abspath('./'), 'param_card_heft.dat')
mued      = test_model('mued',      path.abspath('./'), 'param_card_mued.dat')
mssm      = test_model('mssm',      path.abspath('./'), 'param_card_mssm.dat')
qagc      = test_model('qagc',      path.abspath('./'), 'param_card_qagc.dat')
qcd_d6    = test_model('qcd_d6',    path.abspath('./'), 'param_card_qcd_d6.dat')
DM_Sherpa = test_model('DM_Sherpa', path.abspath('./'), 'param_card_DM_Sherpa.dat')
