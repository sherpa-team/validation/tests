# This file was automatically created by FeynRules 2.0.25
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Mon 2 Nov 2015 12:19:38


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

EFTS = CouplingOrder(name = 'EFTS',
                     expansion_order = 99,
                     hierarchy = 3)

HIG = CouplingOrder(name = 'HIG',
                    expansion_order = 99,
                    hierarchy = 4)

HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 99,
                    hierarchy = 5)

