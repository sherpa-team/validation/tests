#include "METOOLS/Explicit/Lorentz_Calculator.H"
#include "METOOLS/Currents/C_Scalar.H"
#include "METOOLS/Currents/C_Spinor.H"
#include "METOOLS/Currents/C_Vector.H"
#include "METOOLS/Explicit/Vertex.H"
#include "MODEL/Main/Single_Vertex.H"
#include "ATOOLS/Org/Message.H"
#include "ATOOLS/Org/Exception.H"
#include "ATOOLS/Math/MyComplex.H"


namespace METOOLS {

  template <typename SType>
  class FFFF1_Calculator: public Lorentz_Calculator {
  public:

    typedef std::complex<SType> SComplex;

    const SComplex I = SComplex(0.0,1.0);
    
    FFFF1_Calculator(const Vertex_Key &key):
      Lorentz_Calculator(key) {}

    std::string Label() const { return "FFFF1"; }

    CObject *Evaluate(const CObject_Vector &jj)
    {

// if outgoing index is 0
if (p_v->V()->id.back()==0){
const CSpinor <SType> & j1 = ((jj[0]->Get< CSpinor <SType> >())->B() == -1) ? (*(jj[0]->Get< CSpinor <SType> >())) : (*(jj[0]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j10 = j1[0];
const SComplex & j11 = j1[1];
const SComplex & j12 = j1[2];
const SComplex & j13 = j1[3];
const CSpinor <SType> & j2 = ((jj[1]->Get< CSpinor <SType> >())->B() == 1) ? (*(jj[1]->Get< CSpinor <SType> >())) : (*(jj[1]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j20 = j2[0];
const SComplex & j21 = j2[1];
const SComplex & j22 = j2[2];
const SComplex & j23 = j2[3];
const CSpinor <SType> & j3 = ((jj[2]->Get< CSpinor <SType> >())->B() == -1) ? (*(jj[2]->Get< CSpinor <SType> >())) : (*(jj[2]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j30 = j3[0];
const SComplex & j31 = j3[1];
const SComplex & j32 = j3[2];
const SComplex & j33 = j3[3];
CSpinor<SType>* j0 = NULL;
switch(+(j1.On()<<(2))+(j2.On()<<(4))+(j3.On()<<(6))){
case 84:
return j0;
case 88:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,2);
(*j0)[0] = 0;
(*j0)[1] = 0;
(*j0)[2] = 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 92:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,2);
(*j0)[0] = 0;
(*j0)[1] = 0;
(*j0)[2] = 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 100:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,2);
(*j0)[0] = 0;
(*j0)[1] = 0;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 104:
return j0;
case 108:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,2);
(*j0)[0] = 0;
(*j0)[1] = 0;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 116:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,2);
(*j0)[0] = 0;
(*j0)[1] = 0;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 120:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,2);
(*j0)[0] = 0;
(*j0)[1] = 0;
(*j0)[2] = 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 124:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,2);
(*j0)[0] = 0;
(*j0)[1] = 0;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30 + 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30 + 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 148:
return j0;
case 152:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,1);
(*j0)[0] = -2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = 0;
(*j0)[3] = 0;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 156:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,1);
(*j0)[0] = -2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = 0;
(*j0)[3] = 0;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 164:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,1);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33;
(*j0)[2] = 0;
(*j0)[3] = 0;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 168:
return j0;
case 172:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,1);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33;
(*j0)[2] = 0;
(*j0)[3] = 0;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 180:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,1);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33;
(*j0)[2] = 0;
(*j0)[3] = 0;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 184:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,1);
(*j0)[0] = -2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = 0;
(*j0)[3] = 0;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 188:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,1);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33 - 2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33 + 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = 0;
(*j0)[3] = 0;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 212:
return j0;
case 216:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,3);
(*j0)[0] = -2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 220:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,3);
(*j0)[0] = -2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 228:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,3);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 232:
return j0;
case 236:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,3);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 244:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,3);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 248:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,3);
(*j0)[0] = -2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
case 252:
j0 = CSpinor<SType>::New(m_r[0],-1,0,0,0,0,3);
(*j0)[0] = 2.0*j10*j22*j32 + 2.0*j10*j23*j33 - 2.0*j12*j21*j33 + 2.0*j13*j21*j32;
(*j0)[1] = 2.0*j11*j22*j32 + 2.0*j11*j23*j33 + 2.0*j12*j20*j33 - 2.0*j13*j20*j32;
(*j0)[2] = -2.0*j10*j23*j31 + 2.0*j11*j23*j30 + 2.0*j12*j20*j30 + 2.0*j12*j21*j31;
(*j0)[3] = 2.0*j10*j22*j31 - 2.0*j11*j22*j30 + 2.0*j13*j20*j30 + 2.0*j13*j21*j31;
j0->SetS(j1.S()|j2.S()|j3.S());
return j0;
default:
 THROW(fatal_error, "Massless spinor optimization error in Lorentz calculator");
}

}

// if outgoing index is 1
if (p_v->V()->id.back()==1){
const CSpinor <SType> & j0 = ((jj[2]->Get< CSpinor <SType> >())->B() == 1) ? (*(jj[2]->Get< CSpinor <SType> >())) : (*(jj[2]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j00 = j0[0];
const SComplex & j01 = j0[1];
const SComplex & j02 = j0[2];
const SComplex & j03 = j0[3];
const CSpinor <SType> & j2 = ((jj[0]->Get< CSpinor <SType> >())->B() == 1) ? (*(jj[0]->Get< CSpinor <SType> >())) : (*(jj[0]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j20 = j2[0];
const SComplex & j21 = j2[1];
const SComplex & j22 = j2[2];
const SComplex & j23 = j2[3];
const CSpinor <SType> & j3 = ((jj[1]->Get< CSpinor <SType> >())->B() == -1) ? (*(jj[1]->Get< CSpinor <SType> >())) : (*(jj[1]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j30 = j3[0];
const SComplex & j31 = j3[1];
const SComplex & j32 = j3[2];
const SComplex & j33 = j3[3];
CSpinor<SType>* j1 = NULL;
switch(+(j0.On()<<(0))+(j2.On()<<(4))+(j3.On()<<(6))){
case 81:
return j1;
case 82:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,2);
(*j1)[0] = 0;
(*j1)[1] = 0;
(*j1)[2] = 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 83:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,2);
(*j1)[0] = 0;
(*j1)[1] = 0;
(*j1)[2] = 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 97:
return j1;
case 98:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,1);
(*j1)[0] = -2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = 0;
(*j1)[3] = 0;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 99:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,1);
(*j1)[0] = -2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = 0;
(*j1)[3] = 0;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 113:
return j1;
case 114:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,3);
(*j1)[0] = -2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 115:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,3);
(*j1)[0] = -2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 145:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,2);
(*j1)[0] = 0;
(*j1)[1] = 0;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 146:
return j1;
case 147:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,2);
(*j1)[0] = 0;
(*j1)[1] = 0;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 161:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,1);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33;
(*j1)[2] = 0;
(*j1)[3] = 0;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 162:
return j1;
case 163:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,1);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33;
(*j1)[2] = 0;
(*j1)[3] = 0;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 177:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,3);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 178:
return j1;
case 179:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,3);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 209:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,2);
(*j1)[0] = 0;
(*j1)[1] = 0;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 210:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,2);
(*j1)[0] = 0;
(*j1)[1] = 0;
(*j1)[2] = 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 211:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,2);
(*j1)[0] = 0;
(*j1)[1] = 0;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33 + 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32 + 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 225:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,1);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33;
(*j1)[2] = 0;
(*j1)[3] = 0;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 226:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,1);
(*j1)[0] = -2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = 0;
(*j1)[3] = 0;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 227:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,1);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33 - 2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33 + 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = 0;
(*j1)[3] = 0;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 241:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,3);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 242:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,3);
(*j1)[0] = -2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
case 243:
j1 = CSpinor<SType>::New(m_r[1],1,0,0,0,0,3);
(*j1)[0] = 2.0*j00*j22*j32 + 2.0*j00*j23*j33 - 2.0*j02*j23*j31 + 2.0*j03*j22*j31;
(*j1)[1] = 2.0*j01*j22*j32 + 2.0*j01*j23*j33 + 2.0*j02*j23*j30 - 2.0*j03*j22*j30;
(*j1)[2] = -2.0*j00*j21*j33 + 2.0*j01*j20*j33 + 2.0*j02*j20*j30 + 2.0*j02*j21*j31;
(*j1)[3] = 2.0*j00*j21*j32 - 2.0*j01*j20*j32 + 2.0*j03*j20*j30 + 2.0*j03*j21*j31;
j1->SetS(j0.S()|j2.S()|j3.S());
return j1;
default:
 THROW(fatal_error, "Massless spinor optimization error in Lorentz calculator");
}

}

// if outgoing index is 2
if (p_v->V()->id.back()==2){
const CSpinor <SType> & j0 = ((jj[1]->Get< CSpinor <SType> >())->B() == 1) ? (*(jj[1]->Get< CSpinor <SType> >())) : (*(jj[1]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j00 = j0[0];
const SComplex & j01 = j0[1];
const SComplex & j02 = j0[2];
const SComplex & j03 = j0[3];
const CSpinor <SType> & j1 = ((jj[2]->Get< CSpinor <SType> >())->B() == -1) ? (*(jj[2]->Get< CSpinor <SType> >())) : (*(jj[2]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j10 = j1[0];
const SComplex & j11 = j1[1];
const SComplex & j12 = j1[2];
const SComplex & j13 = j1[3];
const CSpinor <SType> & j3 = ((jj[0]->Get< CSpinor <SType> >())->B() == -1) ? (*(jj[0]->Get< CSpinor <SType> >())) : (*(jj[0]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j30 = j3[0];
const SComplex & j31 = j3[1];
const SComplex & j32 = j3[2];
const SComplex & j33 = j3[3];
CSpinor<SType>* j2 = NULL;
switch(+(j0.On()<<(0))+(j1.On()<<(2))+(j3.On()<<(6))){
case 69:
return j2;
case 70:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,2);
(*j2)[0] = 0;
(*j2)[1] = 0;
(*j2)[2] = 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = -2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 71:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,2);
(*j2)[0] = 0;
(*j2)[1] = 0;
(*j2)[2] = 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = -2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 73:
return j2;
case 74:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,1);
(*j2)[0] = 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 0;
(*j2)[3] = 0;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 75:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,1);
(*j2)[0] = 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 0;
(*j2)[3] = 0;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 77:
return j2;
case 78:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,3);
(*j2)[0] = 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = -2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 79:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,3);
(*j2)[0] = 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = -2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 133:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,2);
(*j2)[0] = 0;
(*j2)[1] = 0;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 134:
return j2;
case 135:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,2);
(*j2)[0] = 0;
(*j2)[1] = 0;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 137:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,1);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32;
(*j2)[2] = 0;
(*j2)[3] = 0;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 138:
return j2;
case 139:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,1);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32;
(*j2)[2] = 0;
(*j2)[3] = 0;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 141:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,3);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 142:
return j2;
case 143:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,3);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 197:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,2);
(*j2)[0] = 0;
(*j2)[1] = 0;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 198:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,2);
(*j2)[0] = 0;
(*j2)[1] = 0;
(*j2)[2] = 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = -2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 199:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,2);
(*j2)[0] = 0;
(*j2)[1] = 0;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32 + 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33 - 2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 201:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,1);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32;
(*j2)[2] = 0;
(*j2)[3] = 0;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 202:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,1);
(*j2)[0] = 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 0;
(*j2)[3] = 0;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 203:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,1);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32 + 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32 + 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 0;
(*j2)[3] = 0;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 205:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,3);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 206:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,3);
(*j2)[0] = 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = -2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
case 207:
j2 = CSpinor<SType>::New(m_r[2],-1,0,0,0,0,3);
(*j2)[0] = 2.0*j01*j12*j33 - 2.0*j01*j13*j32 + 2.0*j02*j12*j30 + 2.0*j03*j13*j30;
(*j2)[1] = -2.0*j00*j12*j33 + 2.0*j00*j13*j32 + 2.0*j02*j12*j31 + 2.0*j03*j13*j31;
(*j2)[2] = 2.0*j00*j10*j32 + 2.0*j01*j11*j32 + 2.0*j03*j10*j31 - 2.0*j03*j11*j30;
(*j2)[3] = 2.0*j00*j10*j33 + 2.0*j01*j11*j33 - 2.0*j02*j10*j31 + 2.0*j02*j11*j30;
j2->SetS(j0.S()|j1.S()|j3.S());
return j2;
default:
 THROW(fatal_error, "Massless spinor optimization error in Lorentz calculator");
}

}

// if outgoing index is 3
if (p_v->V()->id.back()==3){
const CSpinor <SType> & j0 = ((jj[0]->Get< CSpinor <SType> >())->B() == 1) ? (*(jj[0]->Get< CSpinor <SType> >())) : (*(jj[0]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j00 = j0[0];
const SComplex & j01 = j0[1];
const SComplex & j02 = j0[2];
const SComplex & j03 = j0[3];
const CSpinor <SType> & j1 = ((jj[1]->Get< CSpinor <SType> >())->B() == -1) ? (*(jj[1]->Get< CSpinor <SType> >())) : (*(jj[1]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j10 = j1[0];
const SComplex & j11 = j1[1];
const SComplex & j12 = j1[2];
const SComplex & j13 = j1[3];
const CSpinor <SType> & j2 = ((jj[2]->Get< CSpinor <SType> >())->B() == 1) ? (*(jj[2]->Get< CSpinor <SType> >())) : (*(jj[2]->Get< CSpinor <SType> >())).CConj() ;
const SComplex & j20 = j2[0];
const SComplex & j21 = j2[1];
const SComplex & j22 = j2[2];
const SComplex & j23 = j2[3];
CSpinor<SType>* j3 = NULL;
switch(+(j0.On()<<(0))+(j1.On()<<(2))+(j2.On()<<(4))){
case 21:
return j3;
case 22:
return j3;
case 23:
return j3;
case 25:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,2);
(*j3)[0] = 0;
(*j3)[1] = 0;
(*j3)[2] = 2.0*j00*j13*j21 - 2.0*j01*j13*j20;
(*j3)[3] = -2.0*j00*j12*j21 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 26:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,1);
(*j3)[0] = 2.0*j02*j12*j20 + 2.0*j03*j13*j20;
(*j3)[1] = 2.0*j02*j12*j21 + 2.0*j03*j13*j21;
(*j3)[2] = 0;
(*j3)[3] = 0;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 27:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,3);
(*j3)[0] = 2.0*j02*j12*j20 + 2.0*j03*j13*j20;
(*j3)[1] = 2.0*j02*j12*j21 + 2.0*j03*j13*j21;
(*j3)[2] = 2.0*j00*j13*j21 - 2.0*j01*j13*j20;
(*j3)[3] = -2.0*j00*j12*j21 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 29:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,2);
(*j3)[0] = 0;
(*j3)[1] = 0;
(*j3)[2] = 2.0*j00*j13*j21 - 2.0*j01*j13*j20;
(*j3)[3] = -2.0*j00*j12*j21 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 30:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,1);
(*j3)[0] = 2.0*j02*j12*j20 + 2.0*j03*j13*j20;
(*j3)[1] = 2.0*j02*j12*j21 + 2.0*j03*j13*j21;
(*j3)[2] = 0;
(*j3)[3] = 0;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 31:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,3);
(*j3)[0] = 2.0*j02*j12*j20 + 2.0*j03*j13*j20;
(*j3)[1] = 2.0*j02*j12*j21 + 2.0*j03*j13*j21;
(*j3)[2] = 2.0*j00*j13*j21 - 2.0*j01*j13*j20;
(*j3)[3] = -2.0*j00*j12*j21 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 37:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,2);
(*j3)[0] = 0;
(*j3)[1] = 0;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j01*j11*j22;
(*j3)[3] = 2.0*j00*j10*j23 + 2.0*j01*j11*j23;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 38:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,1);
(*j3)[0] = 2.0*j02*j11*j23 - 2.0*j03*j11*j22;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j03*j10*j22;
(*j3)[2] = 0;
(*j3)[3] = 0;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 39:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,3);
(*j3)[0] = 2.0*j02*j11*j23 - 2.0*j03*j11*j22;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j03*j10*j22;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j01*j11*j22;
(*j3)[3] = 2.0*j00*j10*j23 + 2.0*j01*j11*j23;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 41:
return j3;
case 42:
return j3;
case 43:
return j3;
case 45:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,2);
(*j3)[0] = 0;
(*j3)[1] = 0;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j01*j11*j22;
(*j3)[3] = 2.0*j00*j10*j23 + 2.0*j01*j11*j23;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 46:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,1);
(*j3)[0] = 2.0*j02*j11*j23 - 2.0*j03*j11*j22;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j03*j10*j22;
(*j3)[2] = 0;
(*j3)[3] = 0;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 47:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,3);
(*j3)[0] = 2.0*j02*j11*j23 - 2.0*j03*j11*j22;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j03*j10*j22;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j01*j11*j22;
(*j3)[3] = 2.0*j00*j10*j23 + 2.0*j01*j11*j23;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 53:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,2);
(*j3)[0] = 0;
(*j3)[1] = 0;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j01*j11*j22;
(*j3)[3] = 2.0*j00*j10*j23 + 2.0*j01*j11*j23;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 54:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,1);
(*j3)[0] = 2.0*j02*j11*j23 - 2.0*j03*j11*j22;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j03*j10*j22;
(*j3)[2] = 0;
(*j3)[3] = 0;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 55:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,3);
(*j3)[0] = 2.0*j02*j11*j23 - 2.0*j03*j11*j22;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j03*j10*j22;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j01*j11*j22;
(*j3)[3] = 2.0*j00*j10*j23 + 2.0*j01*j11*j23;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 57:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,2);
(*j3)[0] = 0;
(*j3)[1] = 0;
(*j3)[2] = 2.0*j00*j13*j21 - 2.0*j01*j13*j20;
(*j3)[3] = -2.0*j00*j12*j21 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 58:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,1);
(*j3)[0] = 2.0*j02*j12*j20 + 2.0*j03*j13*j20;
(*j3)[1] = 2.0*j02*j12*j21 + 2.0*j03*j13*j21;
(*j3)[2] = 0;
(*j3)[3] = 0;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 59:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,3);
(*j3)[0] = 2.0*j02*j12*j20 + 2.0*j03*j13*j20;
(*j3)[1] = 2.0*j02*j12*j21 + 2.0*j03*j13*j21;
(*j3)[2] = 2.0*j00*j13*j21 - 2.0*j01*j13*j20;
(*j3)[3] = -2.0*j00*j12*j21 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 61:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,2);
(*j3)[0] = 0;
(*j3)[1] = 0;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j00*j13*j21 + 2.0*j01*j11*j22 - 2.0*j01*j13*j20;
(*j3)[3] = 2.0*j00*j10*j23 - 2.0*j00*j12*j21 + 2.0*j01*j11*j23 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 62:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,1);
(*j3)[0] = 2.0*j02*j11*j23 + 2.0*j02*j12*j20 - 2.0*j03*j11*j22 + 2.0*j03*j13*j20;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j02*j12*j21 + 2.0*j03*j10*j22 + 2.0*j03*j13*j21;
(*j3)[2] = 0;
(*j3)[3] = 0;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
case 63:
j3 = CSpinor<SType>::New(m_r[3],1,0,0,0,0,3);
(*j3)[0] = 2.0*j02*j11*j23 + 2.0*j02*j12*j20 - 2.0*j03*j11*j22 + 2.0*j03*j13*j20;
(*j3)[1] = -2.0*j02*j10*j23 + 2.0*j02*j12*j21 + 2.0*j03*j10*j22 + 2.0*j03*j13*j21;
(*j3)[2] = 2.0*j00*j10*j22 + 2.0*j00*j13*j21 + 2.0*j01*j11*j22 - 2.0*j01*j13*j20;
(*j3)[3] = 2.0*j00*j10*j23 - 2.0*j00*j12*j21 + 2.0*j01*j11*j23 + 2.0*j01*j12*j20;
j3->SetS(j0.S()|j1.S()|j2.S());
return j3;
default:
 THROW(fatal_error, "Massless spinor optimization error in Lorentz calculator");
}

}

      THROW(fatal_error, "Internal error in Lorentz calculator");
      return NULL;
    }

  };// end of class FFFF1_Calculator

  template class FFFF1_Calculator<double>;

}// end of namespace METOOLS


using namespace METOOLS;

DECLARE_GETTER(FFFF1_Calculator<double>,"DFFFF1",
	       Lorentz_Calculator,Vertex_Key);
Lorentz_Calculator *ATOOLS::Getter
<Lorentz_Calculator,Vertex_Key,FFFF1_Calculator<double> >::
operator()(const Vertex_Key &key) const
{ return new FFFF1_Calculator<double>(key); }

void ATOOLS::Getter<Lorentz_Calculator,Vertex_Key,
		    FFFF1_Calculator<double> >::
PrintInfo(std::ostream &str,const size_t width) const
{ str<<"FFFF1 vertex"; }
