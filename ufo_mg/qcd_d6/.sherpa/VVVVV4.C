#include "METOOLS/Explicit/Lorentz_Calculator.H"
#include "METOOLS/Currents/C_Scalar.H"
#include "METOOLS/Currents/C_Spinor.H"
#include "METOOLS/Currents/C_Vector.H"
#include "METOOLS/Explicit/Vertex.H"
#include "MODEL/Main/Single_Vertex.H"
#include "ATOOLS/Org/Message.H"
#include "ATOOLS/Org/Exception.H"
#include "ATOOLS/Math/MyComplex.H"


namespace METOOLS {

  template <typename SType>
  class VVVVV4_Calculator: public Lorentz_Calculator {
  public:

    typedef std::complex<SType> SComplex;

    const SComplex I = SComplex(0.0,1.0);
    
    VVVVV4_Calculator(const Vertex_Key &key):
      Lorentz_Calculator(key) {}

    std::string Label() const { return "VVVVV4"; }

    CObject *Evaluate(const CObject_Vector &jj)
    {

// if outgoing index is 0
if (p_v->V()->id.back()==0){
const CVec4 <SType> & j1 = *(jj[0]->Get< CVec4 <SType> >());
const SComplex & j10 = j1[0];
const SComplex & j11 = j1[ATOOLS::Spinor<SType>::R1()];
const SComplex & j12 = j1[ATOOLS::Spinor<SType>::R2()];
const SComplex & j13 = j1[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p1 = p_v->J(0)->P();
const double& p10 = p1[0];
const double& p11 = p1[ATOOLS::Spinor<SType>::R1()];
const double& p12 = p1[ATOOLS::Spinor<SType>::R2()];
const double& p13 = p1[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j2 = *(jj[1]->Get< CVec4 <SType> >());
const SComplex & j20 = j2[0];
const SComplex & j21 = j2[ATOOLS::Spinor<SType>::R1()];
const SComplex & j22 = j2[ATOOLS::Spinor<SType>::R2()];
const SComplex & j23 = j2[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p2 = p_v->J(1)->P();
const double& p20 = p2[0];
const double& p21 = p2[ATOOLS::Spinor<SType>::R1()];
const double& p22 = p2[ATOOLS::Spinor<SType>::R2()];
const double& p23 = p2[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j3 = *(jj[2]->Get< CVec4 <SType> >());
const SComplex & j30 = j3[0];
const SComplex & j31 = j3[ATOOLS::Spinor<SType>::R1()];
const SComplex & j32 = j3[ATOOLS::Spinor<SType>::R2()];
const SComplex & j33 = j3[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p3 = p_v->J(2)->P();
const double& p30 = p3[0];
const double& p31 = p3[ATOOLS::Spinor<SType>::R1()];
const double& p32 = p3[ATOOLS::Spinor<SType>::R2()];
const double& p33 = p3[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j4 = *(jj[3]->Get< CVec4 <SType> >());
const SComplex & j40 = j4[0];
const SComplex & j41 = j4[ATOOLS::Spinor<SType>::R1()];
const SComplex & j42 = j4[ATOOLS::Spinor<SType>::R2()];
const SComplex & j43 = j4[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p4 = p_v->J(3)->P();
const double& p40 = p4[0];
const double& p41 = p4[ATOOLS::Spinor<SType>::R1()];
const double& p42 = p4[ATOOLS::Spinor<SType>::R2()];
const double& p43 = p4[ATOOLS::Spinor<SType>::R3()];
ATOOLS::Vec4D p0 = -p1-p2-p3-p4;
const double& p00 = p0[0];
const double& p01 = p0[ATOOLS::Spinor<SType>::R1()];
const double& p02 = p0[ATOOLS::Spinor<SType>::R2()];
const double& p03 = p0[ATOOLS::Spinor<SType>::R3()];
CVec4<SType>* j0 = NULL;
j0 = CVec4<SType>::New();
(*j0)[0] = -1.0*j10*j21*j31*j42*p02 - 1.0*j10*j21*j31*j43*p03 + 1.0*j10*j21*j32*j42*p01 + 1.0*j10*j21*j33*j43*p01 + 1.0*j10*j22*j31*j41*p02 - 1.0*j10*j22*j32*j41*p01 - 1.0*j10*j22*j32*j43*p03 + 1.0*j10*j22*j33*j43*p02 + 1.0*j10*j23*j31*j41*p03 + 1.0*j10*j23*j32*j42*p03 - 1.0*j10*j23*j33*j41*p01 - 1.0*j10*j23*j33*j42*p02 + 1.0*j11*j20*j32*j41*p02 - 1.0*j11*j20*j32*j42*p01 + 1.0*j11*j20*j33*j41*p03 - 1.0*j11*j20*j33*j43*p01 + 1.0*j11*j21*j30*j42*p02 + 1.0*j11*j21*j30*j43*p03 - 1.0*j11*j21*j32*j40*p02 - 1.0*j11*j21*j33*j40*p03 - 1.0*j11*j22*j30*j41*p02 + 1.0*j11*j22*j32*j40*p01 - 1.0*j11*j23*j30*j41*p03 + 1.0*j11*j23*j33*j40*p01 - 1.0*j12*j20*j31*j41*p02 + 1.0*j12*j20*j31*j42*p01 + 1.0*j12*j20*j33*j42*p03 - 1.0*j12*j20*j33*j43*p02 - 1.0*j12*j21*j30*j42*p01 + 1.0*j12*j21*j31*j40*p02 + 1.0*j12*j22*j30*j41*p01 + 1.0*j12*j22*j30*j43*p03 - 1.0*j12*j22*j31*j40*p01 - 1.0*j12*j22*j33*j40*p03 - 1.0*j12*j23*j30*j42*p03 + 1.0*j12*j23*j33*j40*p02 - 1.0*j13*j20*j31*j41*p03 + 1.0*j13*j20*j31*j43*p01 - 1.0*j13*j20*j32*j42*p03 + 1.0*j13*j20*j32*j43*p02 - 1.0*j13*j21*j30*j43*p01 + 1.0*j13*j21*j31*j40*p03 - 1.0*j13*j22*j30*j43*p02 + 1.0*j13*j22*j32*j40*p03 + 1.0*j13*j23*j30*j41*p01 + 1.0*j13*j23*j30*j42*p02 - 1.0*j13*j23*j31*j40*p01 - 1.0*j13*j23*j32*j40*p02;
(*j0)[ATOOLS::Spinor<SType>::R1()] = -1.0*j10*j20*j31*j42*p02 - 1.0*j10*j20*j31*j43*p03 + 1.0*j10*j20*j32*j41*p02 + 1.0*j10*j20*j33*j41*p03 - 1.0*j10*j21*j32*j40*p02 + 1.0*j10*j21*j32*j42*p00 - 1.0*j10*j21*j33*j40*p03 + 1.0*j10*j21*j33*j43*p00 + 1.0*j10*j22*j31*j40*p02 - 1.0*j10*j22*j32*j41*p00 + 1.0*j10*j23*j31*j40*p03 - 1.0*j10*j23*j33*j41*p00 + 1.0*j11*j20*j30*j42*p02 + 1.0*j11*j20*j30*j43*p03 - 1.0*j11*j20*j32*j42*p00 - 1.0*j11*j20*j33*j43*p00 - 1.0*j11*j22*j30*j40*p02 + 1.0*j11*j22*j32*j40*p00 - 1.0*j11*j22*j32*j43*p03 + 1.0*j11*j22*j33*j43*p02 - 1.0*j11*j23*j30*j40*p03 + 1.0*j11*j23*j32*j42*p03 + 1.0*j11*j23*j33*j40*p00 - 1.0*j11*j23*j33*j42*p02 - 1.0*j12*j20*j30*j41*p02 + 1.0*j12*j20*j31*j42*p00 + 1.0*j12*j21*j30*j40*p02 - 1.0*j12*j21*j30*j42*p00 + 1.0*j12*j21*j33*j42*p03 - 1.0*j12*j21*j33*j43*p02 + 1.0*j12*j22*j30*j41*p00 - 1.0*j12*j22*j31*j40*p00 + 1.0*j12*j22*j31*j43*p03 - 1.0*j12*j22*j33*j41*p03 - 1.0*j12*j23*j31*j42*p03 + 1.0*j12*j23*j33*j41*p02 - 1.0*j13*j20*j30*j41*p03 + 1.0*j13*j20*j31*j43*p00 + 1.0*j13*j21*j30*j40*p03 - 1.0*j13*j21*j30*j43*p00 - 1.0*j13*j21*j32*j42*p03 + 1.0*j13*j21*j32*j43*p02 - 1.0*j13*j22*j31*j43*p02 + 1.0*j13*j22*j32*j41*p03 + 1.0*j13*j23*j30*j41*p00 - 1.0*j13*j23*j31*j40*p00 + 1.0*j13*j23*j31*j42*p02 - 1.0*j13*j23*j32*j41*p02;
(*j0)[ATOOLS::Spinor<SType>::R2()] = 1.0*j10*j20*j31*j42*p01 - 1.0*j10*j20*j32*j41*p01 - 1.0*j10*j20*j32*j43*p03 + 1.0*j10*j20*j33*j42*p03 - 1.0*j10*j21*j31*j42*p00 + 1.0*j10*j21*j32*j40*p01 - 1.0*j10*j22*j31*j40*p01 + 1.0*j10*j22*j31*j41*p00 - 1.0*j10*j22*j33*j40*p03 + 1.0*j10*j22*j33*j43*p00 + 1.0*j10*j23*j32*j40*p03 - 1.0*j10*j23*j33*j42*p00 - 1.0*j11*j20*j30*j42*p01 + 1.0*j11*j20*j32*j41*p00 + 1.0*j11*j21*j30*j42*p00 - 1.0*j11*j21*j32*j40*p00 + 1.0*j11*j21*j32*j43*p03 - 1.0*j11*j21*j33*j42*p03 + 1.0*j11*j22*j30*j40*p01 - 1.0*j11*j22*j30*j41*p00 + 1.0*j11*j22*j33*j41*p03 - 1.0*j11*j22*j33*j43*p01 - 1.0*j11*j23*j32*j41*p03 + 1.0*j11*j23*j33*j42*p01 + 1.0*j12*j20*j30*j41*p01 + 1.0*j12*j20*j30*j43*p03 - 1.0*j12*j20*j31*j41*p00 - 1.0*j12*j20*j33*j43*p00 - 1.0*j12*j21*j30*j40*p01 + 1.0*j12*j21*j31*j40*p00 - 1.0*j12*j21*j31*j43*p03 + 1.0*j12*j21*j33*j43*p01 - 1.0*j12*j23*j30*j40*p03 + 1.0*j12*j23*j31*j41*p03 + 1.0*j12*j23*j33*j40*p00 - 1.0*j12*j23*j33*j41*p01 - 1.0*j13*j20*j30*j42*p03 + 1.0*j13*j20*j32*j43*p00 + 1.0*j13*j21*j31*j42*p03 - 1.0*j13*j21*j32*j43*p01 + 1.0*j13*j22*j30*j40*p03 - 1.0*j13*j22*j30*j43*p00 - 1.0*j13*j22*j31*j41*p03 + 1.0*j13*j22*j31*j43*p01 + 1.0*j13*j23*j30*j42*p00 - 1.0*j13*j23*j31*j42*p01 - 1.0*j13*j23*j32*j40*p00 + 1.0*j13*j23*j32*j41*p01;
(*j0)[ATOOLS::Spinor<SType>::R3()] = 1.0*j10*j20*j31*j43*p01 + 1.0*j10*j20*j32*j43*p02 - 1.0*j10*j20*j33*j41*p01 - 1.0*j10*j20*j33*j42*p02 - 1.0*j10*j21*j31*j43*p00 + 1.0*j10*j21*j33*j40*p01 - 1.0*j10*j22*j32*j43*p00 + 1.0*j10*j22*j33*j40*p02 - 1.0*j10*j23*j31*j40*p01 + 1.0*j10*j23*j31*j41*p00 - 1.0*j10*j23*j32*j40*p02 + 1.0*j10*j23*j32*j42*p00 - 1.0*j11*j20*j30*j43*p01 + 1.0*j11*j20*j33*j41*p00 + 1.0*j11*j21*j30*j43*p00 - 1.0*j11*j21*j32*j43*p02 - 1.0*j11*j21*j33*j40*p00 + 1.0*j11*j21*j33*j42*p02 + 1.0*j11*j22*j32*j43*p01 - 1.0*j11*j22*j33*j41*p02 + 1.0*j11*j23*j30*j40*p01 - 1.0*j11*j23*j30*j41*p00 + 1.0*j11*j23*j32*j41*p02 - 1.0*j11*j23*j32*j42*p01 - 1.0*j12*j20*j30*j43*p02 + 1.0*j12*j20*j33*j42*p00 + 1.0*j12*j21*j31*j43*p02 - 1.0*j12*j21*j33*j42*p01 + 1.0*j12*j22*j30*j43*p00 - 1.0*j12*j22*j31*j43*p01 - 1.0*j12*j22*j33*j40*p00 + 1.0*j12*j22*j33*j41*p01 + 1.0*j12*j23*j30*j40*p02 - 1.0*j12*j23*j30*j42*p00 - 1.0*j12*j23*j31*j41*p02 + 1.0*j12*j23*j31*j42*p01 + 1.0*j13*j20*j30*j41*p01 + 1.0*j13*j20*j30*j42*p02 - 1.0*j13*j20*j31*j41*p00 - 1.0*j13*j20*j32*j42*p00 - 1.0*j13*j21*j30*j40*p01 + 1.0*j13*j21*j31*j40*p00 - 1.0*j13*j21*j31*j42*p02 + 1.0*j13*j21*j32*j42*p01 - 1.0*j13*j22*j30*j40*p02 + 1.0*j13*j22*j31*j41*p02 + 1.0*j13*j22*j32*j40*p00 - 1.0*j13*j22*j32*j41*p01;
j0->SetS(j1.S()|j2.S()|j3.S()|j4.S());
return j0;

}

// if outgoing index is 1
if (p_v->V()->id.back()==1){
const CVec4 <SType> & j0 = *(jj[3]->Get< CVec4 <SType> >());
const SComplex & j00 = j0[0];
const SComplex & j01 = j0[ATOOLS::Spinor<SType>::R1()];
const SComplex & j02 = j0[ATOOLS::Spinor<SType>::R2()];
const SComplex & j03 = j0[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p0 = p_v->J(3)->P();
const double& p00 = p0[0];
const double& p01 = p0[ATOOLS::Spinor<SType>::R1()];
const double& p02 = p0[ATOOLS::Spinor<SType>::R2()];
const double& p03 = p0[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j2 = *(jj[0]->Get< CVec4 <SType> >());
const SComplex & j20 = j2[0];
const SComplex & j21 = j2[ATOOLS::Spinor<SType>::R1()];
const SComplex & j22 = j2[ATOOLS::Spinor<SType>::R2()];
const SComplex & j23 = j2[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p2 = p_v->J(0)->P();
const double& p20 = p2[0];
const double& p21 = p2[ATOOLS::Spinor<SType>::R1()];
const double& p22 = p2[ATOOLS::Spinor<SType>::R2()];
const double& p23 = p2[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j3 = *(jj[1]->Get< CVec4 <SType> >());
const SComplex & j30 = j3[0];
const SComplex & j31 = j3[ATOOLS::Spinor<SType>::R1()];
const SComplex & j32 = j3[ATOOLS::Spinor<SType>::R2()];
const SComplex & j33 = j3[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p3 = p_v->J(1)->P();
const double& p30 = p3[0];
const double& p31 = p3[ATOOLS::Spinor<SType>::R1()];
const double& p32 = p3[ATOOLS::Spinor<SType>::R2()];
const double& p33 = p3[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j4 = *(jj[2]->Get< CVec4 <SType> >());
const SComplex & j40 = j4[0];
const SComplex & j41 = j4[ATOOLS::Spinor<SType>::R1()];
const SComplex & j42 = j4[ATOOLS::Spinor<SType>::R2()];
const SComplex & j43 = j4[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p4 = p_v->J(2)->P();
const double& p40 = p4[0];
const double& p41 = p4[ATOOLS::Spinor<SType>::R1()];
const double& p42 = p4[ATOOLS::Spinor<SType>::R2()];
const double& p43 = p4[ATOOLS::Spinor<SType>::R3()];
ATOOLS::Vec4D p1 = -p0-p2-p3-p4;
const double& p10 = p1[0];
const double& p11 = p1[ATOOLS::Spinor<SType>::R1()];
const double& p12 = p1[ATOOLS::Spinor<SType>::R2()];
const double& p13 = p1[ATOOLS::Spinor<SType>::R3()];
CVec4<SType>* j1 = NULL;
j1 = CVec4<SType>::New();
(*j1)[0] = -1.0*j00*j21*j31*j42*p02 - 1.0*j00*j21*j31*j43*p03 + 1.0*j00*j21*j32*j42*p01 + 1.0*j00*j21*j33*j43*p01 + 1.0*j00*j22*j31*j41*p02 - 1.0*j00*j22*j32*j41*p01 - 1.0*j00*j22*j32*j43*p03 + 1.0*j00*j22*j33*j43*p02 + 1.0*j00*j23*j31*j41*p03 + 1.0*j00*j23*j32*j42*p03 - 1.0*j00*j23*j33*j41*p01 - 1.0*j00*j23*j33*j42*p02 + 1.0*j01*j20*j31*j42*p02 + 1.0*j01*j20*j31*j43*p03 - 1.0*j01*j20*j32*j41*p02 - 1.0*j01*j20*j33*j41*p03 + 1.0*j01*j21*j32*j40*p02 - 1.0*j01*j21*j32*j42*p00 + 1.0*j01*j21*j33*j40*p03 - 1.0*j01*j21*j33*j43*p00 - 1.0*j01*j22*j31*j40*p02 + 1.0*j01*j22*j32*j41*p00 - 1.0*j01*j23*j31*j40*p03 + 1.0*j01*j23*j33*j41*p00 - 1.0*j02*j20*j31*j42*p01 + 1.0*j02*j20*j32*j41*p01 + 1.0*j02*j20*j32*j43*p03 - 1.0*j02*j20*j33*j42*p03 + 1.0*j02*j21*j31*j42*p00 - 1.0*j02*j21*j32*j40*p01 + 1.0*j02*j22*j31*j40*p01 - 1.0*j02*j22*j31*j41*p00 + 1.0*j02*j22*j33*j40*p03 - 1.0*j02*j22*j33*j43*p00 - 1.0*j02*j23*j32*j40*p03 + 1.0*j02*j23*j33*j42*p00 - 1.0*j03*j20*j31*j43*p01 - 1.0*j03*j20*j32*j43*p02 + 1.0*j03*j20*j33*j41*p01 + 1.0*j03*j20*j33*j42*p02 + 1.0*j03*j21*j31*j43*p00 - 1.0*j03*j21*j33*j40*p01 + 1.0*j03*j22*j32*j43*p00 - 1.0*j03*j22*j33*j40*p02 + 1.0*j03*j23*j31*j40*p01 - 1.0*j03*j23*j31*j41*p00 + 1.0*j03*j23*j32*j40*p02 - 1.0*j03*j23*j32*j42*p00;
(*j1)[ATOOLS::Spinor<SType>::R1()] = -1.0*j00*j20*j32*j41*p02 + 1.0*j00*j20*j32*j42*p01 - 1.0*j00*j20*j33*j41*p03 + 1.0*j00*j20*j33*j43*p01 - 1.0*j00*j21*j30*j42*p02 - 1.0*j00*j21*j30*j43*p03 + 1.0*j00*j21*j32*j40*p02 + 1.0*j00*j21*j33*j40*p03 + 1.0*j00*j22*j30*j41*p02 - 1.0*j00*j22*j32*j40*p01 + 1.0*j00*j23*j30*j41*p03 - 1.0*j00*j23*j33*j40*p01 + 1.0*j01*j20*j30*j42*p02 + 1.0*j01*j20*j30*j43*p03 - 1.0*j01*j20*j32*j42*p00 - 1.0*j01*j20*j33*j43*p00 - 1.0*j01*j22*j30*j40*p02 + 1.0*j01*j22*j32*j40*p00 - 1.0*j01*j22*j32*j43*p03 + 1.0*j01*j22*j33*j43*p02 - 1.0*j01*j23*j30*j40*p03 + 1.0*j01*j23*j32*j42*p03 + 1.0*j01*j23*j33*j40*p00 - 1.0*j01*j23*j33*j42*p02 - 1.0*j02*j20*j30*j42*p01 + 1.0*j02*j20*j32*j41*p00 + 1.0*j02*j21*j30*j42*p00 - 1.0*j02*j21*j32*j40*p00 + 1.0*j02*j21*j32*j43*p03 - 1.0*j02*j21*j33*j42*p03 + 1.0*j02*j22*j30*j40*p01 - 1.0*j02*j22*j30*j41*p00 + 1.0*j02*j22*j33*j41*p03 - 1.0*j02*j22*j33*j43*p01 - 1.0*j02*j23*j32*j41*p03 + 1.0*j02*j23*j33*j42*p01 - 1.0*j03*j20*j30*j43*p01 + 1.0*j03*j20*j33*j41*p00 + 1.0*j03*j21*j30*j43*p00 - 1.0*j03*j21*j32*j43*p02 - 1.0*j03*j21*j33*j40*p00 + 1.0*j03*j21*j33*j42*p02 + 1.0*j03*j22*j32*j43*p01 - 1.0*j03*j22*j33*j41*p02 + 1.0*j03*j23*j30*j40*p01 - 1.0*j03*j23*j30*j41*p00 + 1.0*j03*j23*j32*j41*p02 - 1.0*j03*j23*j32*j42*p01;
(*j1)[ATOOLS::Spinor<SType>::R2()] = 1.0*j00*j20*j31*j41*p02 - 1.0*j00*j20*j31*j42*p01 - 1.0*j00*j20*j33*j42*p03 + 1.0*j00*j20*j33*j43*p02 + 1.0*j00*j21*j30*j42*p01 - 1.0*j00*j21*j31*j40*p02 - 1.0*j00*j22*j30*j41*p01 - 1.0*j00*j22*j30*j43*p03 + 1.0*j00*j22*j31*j40*p01 + 1.0*j00*j22*j33*j40*p03 + 1.0*j00*j23*j30*j42*p03 - 1.0*j00*j23*j33*j40*p02 - 1.0*j01*j20*j30*j41*p02 + 1.0*j01*j20*j31*j42*p00 + 1.0*j01*j21*j30*j40*p02 - 1.0*j01*j21*j30*j42*p00 + 1.0*j01*j21*j33*j42*p03 - 1.0*j01*j21*j33*j43*p02 + 1.0*j01*j22*j30*j41*p00 - 1.0*j01*j22*j31*j40*p00 + 1.0*j01*j22*j31*j43*p03 - 1.0*j01*j22*j33*j41*p03 - 1.0*j01*j23*j31*j42*p03 + 1.0*j01*j23*j33*j41*p02 + 1.0*j02*j20*j30*j41*p01 + 1.0*j02*j20*j30*j43*p03 - 1.0*j02*j20*j31*j41*p00 - 1.0*j02*j20*j33*j43*p00 - 1.0*j02*j21*j30*j40*p01 + 1.0*j02*j21*j31*j40*p00 - 1.0*j02*j21*j31*j43*p03 + 1.0*j02*j21*j33*j43*p01 - 1.0*j02*j23*j30*j40*p03 + 1.0*j02*j23*j31*j41*p03 + 1.0*j02*j23*j33*j40*p00 - 1.0*j02*j23*j33*j41*p01 - 1.0*j03*j20*j30*j43*p02 + 1.0*j03*j20*j33*j42*p00 + 1.0*j03*j21*j31*j43*p02 - 1.0*j03*j21*j33*j42*p01 + 1.0*j03*j22*j30*j43*p00 - 1.0*j03*j22*j31*j43*p01 - 1.0*j03*j22*j33*j40*p00 + 1.0*j03*j22*j33*j41*p01 + 1.0*j03*j23*j30*j40*p02 - 1.0*j03*j23*j30*j42*p00 - 1.0*j03*j23*j31*j41*p02 + 1.0*j03*j23*j31*j42*p01;
(*j1)[ATOOLS::Spinor<SType>::R3()] = 1.0*j00*j20*j31*j41*p03 - 1.0*j00*j20*j31*j43*p01 + 1.0*j00*j20*j32*j42*p03 - 1.0*j00*j20*j32*j43*p02 + 1.0*j00*j21*j30*j43*p01 - 1.0*j00*j21*j31*j40*p03 + 1.0*j00*j22*j30*j43*p02 - 1.0*j00*j22*j32*j40*p03 - 1.0*j00*j23*j30*j41*p01 - 1.0*j00*j23*j30*j42*p02 + 1.0*j00*j23*j31*j40*p01 + 1.0*j00*j23*j32*j40*p02 - 1.0*j01*j20*j30*j41*p03 + 1.0*j01*j20*j31*j43*p00 + 1.0*j01*j21*j30*j40*p03 - 1.0*j01*j21*j30*j43*p00 - 1.0*j01*j21*j32*j42*p03 + 1.0*j01*j21*j32*j43*p02 - 1.0*j01*j22*j31*j43*p02 + 1.0*j01*j22*j32*j41*p03 + 1.0*j01*j23*j30*j41*p00 - 1.0*j01*j23*j31*j40*p00 + 1.0*j01*j23*j31*j42*p02 - 1.0*j01*j23*j32*j41*p02 - 1.0*j02*j20*j30*j42*p03 + 1.0*j02*j20*j32*j43*p00 + 1.0*j02*j21*j31*j42*p03 - 1.0*j02*j21*j32*j43*p01 + 1.0*j02*j22*j30*j40*p03 - 1.0*j02*j22*j30*j43*p00 - 1.0*j02*j22*j31*j41*p03 + 1.0*j02*j22*j31*j43*p01 + 1.0*j02*j23*j30*j42*p00 - 1.0*j02*j23*j31*j42*p01 - 1.0*j02*j23*j32*j40*p00 + 1.0*j02*j23*j32*j41*p01 + 1.0*j03*j20*j30*j41*p01 + 1.0*j03*j20*j30*j42*p02 - 1.0*j03*j20*j31*j41*p00 - 1.0*j03*j20*j32*j42*p00 - 1.0*j03*j21*j30*j40*p01 + 1.0*j03*j21*j31*j40*p00 - 1.0*j03*j21*j31*j42*p02 + 1.0*j03*j21*j32*j42*p01 - 1.0*j03*j22*j30*j40*p02 + 1.0*j03*j22*j31*j41*p02 + 1.0*j03*j22*j32*j40*p00 - 1.0*j03*j22*j32*j41*p01;
j1->SetS(j0.S()|j2.S()|j3.S()|j4.S());
return j1;

}

// if outgoing index is 2
if (p_v->V()->id.back()==2){
const CVec4 <SType> & j0 = *(jj[2]->Get< CVec4 <SType> >());
const SComplex & j00 = j0[0];
const SComplex & j01 = j0[ATOOLS::Spinor<SType>::R1()];
const SComplex & j02 = j0[ATOOLS::Spinor<SType>::R2()];
const SComplex & j03 = j0[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p0 = p_v->J(2)->P();
const double& p00 = p0[0];
const double& p01 = p0[ATOOLS::Spinor<SType>::R1()];
const double& p02 = p0[ATOOLS::Spinor<SType>::R2()];
const double& p03 = p0[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j1 = *(jj[3]->Get< CVec4 <SType> >());
const SComplex & j10 = j1[0];
const SComplex & j11 = j1[ATOOLS::Spinor<SType>::R1()];
const SComplex & j12 = j1[ATOOLS::Spinor<SType>::R2()];
const SComplex & j13 = j1[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p1 = p_v->J(3)->P();
const double& p10 = p1[0];
const double& p11 = p1[ATOOLS::Spinor<SType>::R1()];
const double& p12 = p1[ATOOLS::Spinor<SType>::R2()];
const double& p13 = p1[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j3 = *(jj[0]->Get< CVec4 <SType> >());
const SComplex & j30 = j3[0];
const SComplex & j31 = j3[ATOOLS::Spinor<SType>::R1()];
const SComplex & j32 = j3[ATOOLS::Spinor<SType>::R2()];
const SComplex & j33 = j3[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p3 = p_v->J(0)->P();
const double& p30 = p3[0];
const double& p31 = p3[ATOOLS::Spinor<SType>::R1()];
const double& p32 = p3[ATOOLS::Spinor<SType>::R2()];
const double& p33 = p3[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j4 = *(jj[1]->Get< CVec4 <SType> >());
const SComplex & j40 = j4[0];
const SComplex & j41 = j4[ATOOLS::Spinor<SType>::R1()];
const SComplex & j42 = j4[ATOOLS::Spinor<SType>::R2()];
const SComplex & j43 = j4[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p4 = p_v->J(1)->P();
const double& p40 = p4[0];
const double& p41 = p4[ATOOLS::Spinor<SType>::R1()];
const double& p42 = p4[ATOOLS::Spinor<SType>::R2()];
const double& p43 = p4[ATOOLS::Spinor<SType>::R3()];
ATOOLS::Vec4D p2 = -p0-p1-p3-p4;
const double& p20 = p2[0];
const double& p21 = p2[ATOOLS::Spinor<SType>::R1()];
const double& p22 = p2[ATOOLS::Spinor<SType>::R2()];
const double& p23 = p2[ATOOLS::Spinor<SType>::R3()];
CVec4<SType>* j2 = NULL;
j2 = CVec4<SType>::New();
(*j2)[0] = 1.0*j00*j11*j32*j41*p02 - 1.0*j00*j11*j32*j42*p01 + 1.0*j00*j11*j33*j41*p03 - 1.0*j00*j11*j33*j43*p01 - 1.0*j00*j12*j31*j41*p02 + 1.0*j00*j12*j31*j42*p01 + 1.0*j00*j12*j33*j42*p03 - 1.0*j00*j12*j33*j43*p02 - 1.0*j00*j13*j31*j41*p03 + 1.0*j00*j13*j31*j43*p01 - 1.0*j00*j13*j32*j42*p03 + 1.0*j00*j13*j32*j43*p02 + 1.0*j01*j10*j31*j42*p02 + 1.0*j01*j10*j31*j43*p03 - 1.0*j01*j10*j32*j41*p02 - 1.0*j01*j10*j33*j41*p03 - 1.0*j01*j11*j30*j42*p02 - 1.0*j01*j11*j30*j43*p03 + 1.0*j01*j11*j32*j42*p00 + 1.0*j01*j11*j33*j43*p00 + 1.0*j01*j12*j30*j41*p02 - 1.0*j01*j12*j31*j42*p00 + 1.0*j01*j13*j30*j41*p03 - 1.0*j01*j13*j31*j43*p00 - 1.0*j02*j10*j31*j42*p01 + 1.0*j02*j10*j32*j41*p01 + 1.0*j02*j10*j32*j43*p03 - 1.0*j02*j10*j33*j42*p03 + 1.0*j02*j11*j30*j42*p01 - 1.0*j02*j11*j32*j41*p00 - 1.0*j02*j12*j30*j41*p01 - 1.0*j02*j12*j30*j43*p03 + 1.0*j02*j12*j31*j41*p00 + 1.0*j02*j12*j33*j43*p00 + 1.0*j02*j13*j30*j42*p03 - 1.0*j02*j13*j32*j43*p00 - 1.0*j03*j10*j31*j43*p01 - 1.0*j03*j10*j32*j43*p02 + 1.0*j03*j10*j33*j41*p01 + 1.0*j03*j10*j33*j42*p02 + 1.0*j03*j11*j30*j43*p01 - 1.0*j03*j11*j33*j41*p00 + 1.0*j03*j12*j30*j43*p02 - 1.0*j03*j12*j33*j42*p00 - 1.0*j03*j13*j30*j41*p01 - 1.0*j03*j13*j30*j42*p02 + 1.0*j03*j13*j31*j41*p00 + 1.0*j03*j13*j32*j42*p00;
(*j2)[ATOOLS::Spinor<SType>::R1()] = 1.0*j00*j10*j31*j42*p02 + 1.0*j00*j10*j31*j43*p03 - 1.0*j00*j10*j32*j42*p01 - 1.0*j00*j10*j33*j43*p01 - 1.0*j00*j11*j30*j42*p02 - 1.0*j00*j11*j30*j43*p03 + 1.0*j00*j11*j32*j40*p02 + 1.0*j00*j11*j33*j40*p03 + 1.0*j00*j12*j30*j42*p01 - 1.0*j00*j12*j31*j40*p02 + 1.0*j00*j13*j30*j43*p01 - 1.0*j00*j13*j31*j40*p03 - 1.0*j01*j10*j32*j40*p02 + 1.0*j01*j10*j32*j42*p00 - 1.0*j01*j10*j33*j40*p03 + 1.0*j01*j10*j33*j43*p00 + 1.0*j01*j12*j30*j40*p02 - 1.0*j01*j12*j30*j42*p00 + 1.0*j01*j12*j33*j42*p03 - 1.0*j01*j12*j33*j43*p02 + 1.0*j01*j13*j30*j40*p03 - 1.0*j01*j13*j30*j43*p00 - 1.0*j01*j13*j32*j42*p03 + 1.0*j01*j13*j32*j43*p02 - 1.0*j02*j10*j31*j42*p00 + 1.0*j02*j10*j32*j40*p01 + 1.0*j02*j11*j30*j42*p00 - 1.0*j02*j11*j32*j40*p00 + 1.0*j02*j11*j32*j43*p03 - 1.0*j02*j11*j33*j42*p03 - 1.0*j02*j12*j30*j40*p01 + 1.0*j02*j12*j31*j40*p00 - 1.0*j02*j12*j31*j43*p03 + 1.0*j02*j12*j33*j43*p01 + 1.0*j02*j13*j31*j42*p03 - 1.0*j02*j13*j32*j43*p01 - 1.0*j03*j10*j31*j43*p00 + 1.0*j03*j10*j33*j40*p01 + 1.0*j03*j11*j30*j43*p00 - 1.0*j03*j11*j32*j43*p02 - 1.0*j03*j11*j33*j40*p00 + 1.0*j03*j11*j33*j42*p02 + 1.0*j03*j12*j31*j43*p02 - 1.0*j03*j12*j33*j42*p01 - 1.0*j03*j13*j30*j40*p01 + 1.0*j03*j13*j31*j40*p00 - 1.0*j03*j13*j31*j42*p02 + 1.0*j03*j13*j32*j42*p01;
(*j2)[ATOOLS::Spinor<SType>::R2()] = -1.0*j00*j10*j31*j41*p02 + 1.0*j00*j10*j32*j41*p01 + 1.0*j00*j10*j32*j43*p03 - 1.0*j00*j10*j33*j43*p02 + 1.0*j00*j11*j30*j41*p02 - 1.0*j00*j11*j32*j40*p01 - 1.0*j00*j12*j30*j41*p01 - 1.0*j00*j12*j30*j43*p03 + 1.0*j00*j12*j31*j40*p01 + 1.0*j00*j12*j33*j40*p03 + 1.0*j00*j13*j30*j43*p02 - 1.0*j00*j13*j32*j40*p03 + 1.0*j01*j10*j31*j40*p02 - 1.0*j01*j10*j32*j41*p00 - 1.0*j01*j11*j30*j40*p02 + 1.0*j01*j11*j32*j40*p00 - 1.0*j01*j11*j32*j43*p03 + 1.0*j01*j11*j33*j43*p02 + 1.0*j01*j12*j30*j41*p00 - 1.0*j01*j12*j31*j40*p00 + 1.0*j01*j12*j31*j43*p03 - 1.0*j01*j12*j33*j41*p03 - 1.0*j01*j13*j31*j43*p02 + 1.0*j01*j13*j32*j41*p03 - 1.0*j02*j10*j31*j40*p01 + 1.0*j02*j10*j31*j41*p00 - 1.0*j02*j10*j33*j40*p03 + 1.0*j02*j10*j33*j43*p00 + 1.0*j02*j11*j30*j40*p01 - 1.0*j02*j11*j30*j41*p00 + 1.0*j02*j11*j33*j41*p03 - 1.0*j02*j11*j33*j43*p01 + 1.0*j02*j13*j30*j40*p03 - 1.0*j02*j13*j30*j43*p00 - 1.0*j02*j13*j31*j41*p03 + 1.0*j02*j13*j31*j43*p01 - 1.0*j03*j10*j32*j43*p00 + 1.0*j03*j10*j33*j40*p02 + 1.0*j03*j11*j32*j43*p01 - 1.0*j03*j11*j33*j41*p02 + 1.0*j03*j12*j30*j43*p00 - 1.0*j03*j12*j31*j43*p01 - 1.0*j03*j12*j33*j40*p00 + 1.0*j03*j12*j33*j41*p01 - 1.0*j03*j13*j30*j40*p02 + 1.0*j03*j13*j31*j41*p02 + 1.0*j03*j13*j32*j40*p00 - 1.0*j03*j13*j32*j41*p01;
(*j2)[ATOOLS::Spinor<SType>::R3()] = -1.0*j00*j10*j31*j41*p03 - 1.0*j00*j10*j32*j42*p03 + 1.0*j00*j10*j33*j41*p01 + 1.0*j00*j10*j33*j42*p02 + 1.0*j00*j11*j30*j41*p03 - 1.0*j00*j11*j33*j40*p01 + 1.0*j00*j12*j30*j42*p03 - 1.0*j00*j12*j33*j40*p02 - 1.0*j00*j13*j30*j41*p01 - 1.0*j00*j13*j30*j42*p02 + 1.0*j00*j13*j31*j40*p01 + 1.0*j00*j13*j32*j40*p02 + 1.0*j01*j10*j31*j40*p03 - 1.0*j01*j10*j33*j41*p00 - 1.0*j01*j11*j30*j40*p03 + 1.0*j01*j11*j32*j42*p03 + 1.0*j01*j11*j33*j40*p00 - 1.0*j01*j11*j33*j42*p02 - 1.0*j01*j12*j31*j42*p03 + 1.0*j01*j12*j33*j41*p02 + 1.0*j01*j13*j30*j41*p00 - 1.0*j01*j13*j31*j40*p00 + 1.0*j01*j13*j31*j42*p02 - 1.0*j01*j13*j32*j41*p02 + 1.0*j02*j10*j32*j40*p03 - 1.0*j02*j10*j33*j42*p00 - 1.0*j02*j11*j32*j41*p03 + 1.0*j02*j11*j33*j42*p01 - 1.0*j02*j12*j30*j40*p03 + 1.0*j02*j12*j31*j41*p03 + 1.0*j02*j12*j33*j40*p00 - 1.0*j02*j12*j33*j41*p01 + 1.0*j02*j13*j30*j42*p00 - 1.0*j02*j13*j31*j42*p01 - 1.0*j02*j13*j32*j40*p00 + 1.0*j02*j13*j32*j41*p01 - 1.0*j03*j10*j31*j40*p01 + 1.0*j03*j10*j31*j41*p00 - 1.0*j03*j10*j32*j40*p02 + 1.0*j03*j10*j32*j42*p00 + 1.0*j03*j11*j30*j40*p01 - 1.0*j03*j11*j30*j41*p00 + 1.0*j03*j11*j32*j41*p02 - 1.0*j03*j11*j32*j42*p01 + 1.0*j03*j12*j30*j40*p02 - 1.0*j03*j12*j30*j42*p00 - 1.0*j03*j12*j31*j41*p02 + 1.0*j03*j12*j31*j42*p01;
j2->SetS(j0.S()|j1.S()|j3.S()|j4.S());
return j2;

}

// if outgoing index is 3
if (p_v->V()->id.back()==3){
const CVec4 <SType> & j0 = *(jj[1]->Get< CVec4 <SType> >());
const SComplex & j00 = j0[0];
const SComplex & j01 = j0[ATOOLS::Spinor<SType>::R1()];
const SComplex & j02 = j0[ATOOLS::Spinor<SType>::R2()];
const SComplex & j03 = j0[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p0 = p_v->J(1)->P();
const double& p00 = p0[0];
const double& p01 = p0[ATOOLS::Spinor<SType>::R1()];
const double& p02 = p0[ATOOLS::Spinor<SType>::R2()];
const double& p03 = p0[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j1 = *(jj[2]->Get< CVec4 <SType> >());
const SComplex & j10 = j1[0];
const SComplex & j11 = j1[ATOOLS::Spinor<SType>::R1()];
const SComplex & j12 = j1[ATOOLS::Spinor<SType>::R2()];
const SComplex & j13 = j1[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p1 = p_v->J(2)->P();
const double& p10 = p1[0];
const double& p11 = p1[ATOOLS::Spinor<SType>::R1()];
const double& p12 = p1[ATOOLS::Spinor<SType>::R2()];
const double& p13 = p1[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j2 = *(jj[3]->Get< CVec4 <SType> >());
const SComplex & j20 = j2[0];
const SComplex & j21 = j2[ATOOLS::Spinor<SType>::R1()];
const SComplex & j22 = j2[ATOOLS::Spinor<SType>::R2()];
const SComplex & j23 = j2[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p2 = p_v->J(3)->P();
const double& p20 = p2[0];
const double& p21 = p2[ATOOLS::Spinor<SType>::R1()];
const double& p22 = p2[ATOOLS::Spinor<SType>::R2()];
const double& p23 = p2[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j4 = *(jj[0]->Get< CVec4 <SType> >());
const SComplex & j40 = j4[0];
const SComplex & j41 = j4[ATOOLS::Spinor<SType>::R1()];
const SComplex & j42 = j4[ATOOLS::Spinor<SType>::R2()];
const SComplex & j43 = j4[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p4 = p_v->J(0)->P();
const double& p40 = p4[0];
const double& p41 = p4[ATOOLS::Spinor<SType>::R1()];
const double& p42 = p4[ATOOLS::Spinor<SType>::R2()];
const double& p43 = p4[ATOOLS::Spinor<SType>::R3()];
ATOOLS::Vec4D p3 = -p0-p1-p2-p4;
const double& p30 = p3[0];
const double& p31 = p3[ATOOLS::Spinor<SType>::R1()];
const double& p32 = p3[ATOOLS::Spinor<SType>::R2()];
const double& p33 = p3[ATOOLS::Spinor<SType>::R3()];
CVec4<SType>* j3 = NULL;
j3 = CVec4<SType>::New();
(*j3)[0] = 1.0*j00*j11*j21*j42*p02 + 1.0*j00*j11*j21*j43*p03 - 1.0*j00*j11*j22*j41*p02 - 1.0*j00*j11*j23*j41*p03 - 1.0*j00*j12*j21*j42*p01 + 1.0*j00*j12*j22*j41*p01 + 1.0*j00*j12*j22*j43*p03 - 1.0*j00*j12*j23*j42*p03 - 1.0*j00*j13*j21*j43*p01 - 1.0*j00*j13*j22*j43*p02 + 1.0*j00*j13*j23*j41*p01 + 1.0*j00*j13*j23*j42*p02 - 1.0*j01*j11*j20*j42*p02 - 1.0*j01*j11*j20*j43*p03 + 1.0*j01*j11*j22*j40*p02 + 1.0*j01*j11*j23*j40*p03 + 1.0*j01*j12*j20*j41*p02 - 1.0*j01*j12*j21*j40*p02 + 1.0*j01*j12*j21*j42*p00 - 1.0*j01*j12*j22*j41*p00 + 1.0*j01*j13*j20*j41*p03 - 1.0*j01*j13*j21*j40*p03 + 1.0*j01*j13*j21*j43*p00 - 1.0*j01*j13*j23*j41*p00 + 1.0*j02*j11*j20*j42*p01 - 1.0*j02*j11*j21*j42*p00 - 1.0*j02*j11*j22*j40*p01 + 1.0*j02*j11*j22*j41*p00 - 1.0*j02*j12*j20*j41*p01 - 1.0*j02*j12*j20*j43*p03 + 1.0*j02*j12*j21*j40*p01 + 1.0*j02*j12*j23*j40*p03 + 1.0*j02*j13*j20*j42*p03 - 1.0*j02*j13*j22*j40*p03 + 1.0*j02*j13*j22*j43*p00 - 1.0*j02*j13*j23*j42*p00 + 1.0*j03*j11*j20*j43*p01 - 1.0*j03*j11*j21*j43*p00 - 1.0*j03*j11*j23*j40*p01 + 1.0*j03*j11*j23*j41*p00 + 1.0*j03*j12*j20*j43*p02 - 1.0*j03*j12*j22*j43*p00 - 1.0*j03*j12*j23*j40*p02 + 1.0*j03*j12*j23*j42*p00 - 1.0*j03*j13*j20*j41*p01 - 1.0*j03*j13*j20*j42*p02 + 1.0*j03*j13*j21*j40*p01 + 1.0*j03*j13*j22*j40*p02;
(*j3)[ATOOLS::Spinor<SType>::R1()] = 1.0*j00*j10*j21*j42*p02 + 1.0*j00*j10*j21*j43*p03 - 1.0*j00*j10*j22*j41*p02 - 1.0*j00*j10*j23*j41*p03 + 1.0*j00*j12*j20*j41*p02 - 1.0*j00*j12*j20*j42*p01 - 1.0*j00*j12*j21*j40*p02 + 1.0*j00*j12*j22*j40*p01 + 1.0*j00*j13*j20*j41*p03 - 1.0*j00*j13*j20*j43*p01 - 1.0*j00*j13*j21*j40*p03 + 1.0*j00*j13*j23*j40*p01 - 1.0*j01*j10*j20*j42*p02 - 1.0*j01*j10*j20*j43*p03 + 1.0*j01*j10*j22*j40*p02 + 1.0*j01*j10*j23*j40*p03 + 1.0*j01*j12*j20*j42*p00 - 1.0*j01*j12*j22*j40*p00 + 1.0*j01*j12*j22*j43*p03 - 1.0*j01*j12*j23*j42*p03 + 1.0*j01*j13*j20*j43*p00 - 1.0*j01*j13*j22*j43*p02 - 1.0*j01*j13*j23*j40*p00 + 1.0*j01*j13*j23*j42*p02 + 1.0*j02*j10*j20*j42*p01 - 1.0*j02*j10*j21*j42*p00 - 1.0*j02*j10*j22*j40*p01 + 1.0*j02*j10*j22*j41*p00 - 1.0*j02*j12*j20*j41*p00 + 1.0*j02*j12*j21*j40*p00 - 1.0*j02*j12*j21*j43*p03 + 1.0*j02*j12*j23*j41*p03 + 1.0*j02*j13*j21*j42*p03 - 1.0*j02*j13*j22*j41*p03 + 1.0*j02*j13*j22*j43*p01 - 1.0*j02*j13*j23*j42*p01 + 1.0*j03*j10*j20*j43*p01 - 1.0*j03*j10*j21*j43*p00 - 1.0*j03*j10*j23*j40*p01 + 1.0*j03*j10*j23*j41*p00 + 1.0*j03*j12*j21*j43*p02 - 1.0*j03*j12*j22*j43*p01 - 1.0*j03*j12*j23*j41*p02 + 1.0*j03*j12*j23*j42*p01 - 1.0*j03*j13*j20*j41*p00 + 1.0*j03*j13*j21*j40*p00 - 1.0*j03*j13*j21*j42*p02 + 1.0*j03*j13*j22*j41*p02;
(*j3)[ATOOLS::Spinor<SType>::R2()] = -1.0*j00*j10*j21*j42*p01 + 1.0*j00*j10*j22*j41*p01 + 1.0*j00*j10*j22*j43*p03 - 1.0*j00*j10*j23*j42*p03 - 1.0*j00*j11*j20*j41*p02 + 1.0*j00*j11*j20*j42*p01 + 1.0*j00*j11*j21*j40*p02 - 1.0*j00*j11*j22*j40*p01 + 1.0*j00*j13*j20*j42*p03 - 1.0*j00*j13*j20*j43*p02 - 1.0*j00*j13*j22*j40*p03 + 1.0*j00*j13*j23*j40*p02 + 1.0*j01*j10*j20*j41*p02 - 1.0*j01*j10*j21*j40*p02 + 1.0*j01*j10*j21*j42*p00 - 1.0*j01*j10*j22*j41*p00 - 1.0*j01*j11*j20*j42*p00 + 1.0*j01*j11*j22*j40*p00 - 1.0*j01*j11*j22*j43*p03 + 1.0*j01*j11*j23*j42*p03 - 1.0*j01*j13*j21*j42*p03 + 1.0*j01*j13*j21*j43*p02 + 1.0*j01*j13*j22*j41*p03 - 1.0*j01*j13*j23*j41*p02 - 1.0*j02*j10*j20*j41*p01 - 1.0*j02*j10*j20*j43*p03 + 1.0*j02*j10*j21*j40*p01 + 1.0*j02*j10*j23*j40*p03 + 1.0*j02*j11*j20*j41*p00 - 1.0*j02*j11*j21*j40*p00 + 1.0*j02*j11*j21*j43*p03 - 1.0*j02*j11*j23*j41*p03 + 1.0*j02*j13*j20*j43*p00 - 1.0*j02*j13*j21*j43*p01 - 1.0*j02*j13*j23*j40*p00 + 1.0*j02*j13*j23*j41*p01 + 1.0*j03*j10*j20*j43*p02 - 1.0*j03*j10*j22*j43*p00 - 1.0*j03*j10*j23*j40*p02 + 1.0*j03*j10*j23*j42*p00 - 1.0*j03*j11*j21*j43*p02 + 1.0*j03*j11*j22*j43*p01 + 1.0*j03*j11*j23*j41*p02 - 1.0*j03*j11*j23*j42*p01 - 1.0*j03*j13*j20*j42*p00 + 1.0*j03*j13*j21*j42*p01 + 1.0*j03*j13*j22*j40*p00 - 1.0*j03*j13*j22*j41*p01;
(*j3)[ATOOLS::Spinor<SType>::R3()] = -1.0*j00*j10*j21*j43*p01 - 1.0*j00*j10*j22*j43*p02 + 1.0*j00*j10*j23*j41*p01 + 1.0*j00*j10*j23*j42*p02 - 1.0*j00*j11*j20*j41*p03 + 1.0*j00*j11*j20*j43*p01 + 1.0*j00*j11*j21*j40*p03 - 1.0*j00*j11*j23*j40*p01 - 1.0*j00*j12*j20*j42*p03 + 1.0*j00*j12*j20*j43*p02 + 1.0*j00*j12*j22*j40*p03 - 1.0*j00*j12*j23*j40*p02 + 1.0*j01*j10*j20*j41*p03 - 1.0*j01*j10*j21*j40*p03 + 1.0*j01*j10*j21*j43*p00 - 1.0*j01*j10*j23*j41*p00 - 1.0*j01*j11*j20*j43*p00 + 1.0*j01*j11*j22*j43*p02 + 1.0*j01*j11*j23*j40*p00 - 1.0*j01*j11*j23*j42*p02 + 1.0*j01*j12*j21*j42*p03 - 1.0*j01*j12*j21*j43*p02 - 1.0*j01*j12*j22*j41*p03 + 1.0*j01*j12*j23*j41*p02 + 1.0*j02*j10*j20*j42*p03 - 1.0*j02*j10*j22*j40*p03 + 1.0*j02*j10*j22*j43*p00 - 1.0*j02*j10*j23*j42*p00 - 1.0*j02*j11*j21*j42*p03 + 1.0*j02*j11*j22*j41*p03 - 1.0*j02*j11*j22*j43*p01 + 1.0*j02*j11*j23*j42*p01 - 1.0*j02*j12*j20*j43*p00 + 1.0*j02*j12*j21*j43*p01 + 1.0*j02*j12*j23*j40*p00 - 1.0*j02*j12*j23*j41*p01 - 1.0*j03*j10*j20*j41*p01 - 1.0*j03*j10*j20*j42*p02 + 1.0*j03*j10*j21*j40*p01 + 1.0*j03*j10*j22*j40*p02 + 1.0*j03*j11*j20*j41*p00 - 1.0*j03*j11*j21*j40*p00 + 1.0*j03*j11*j21*j42*p02 - 1.0*j03*j11*j22*j41*p02 + 1.0*j03*j12*j20*j42*p00 - 1.0*j03*j12*j21*j42*p01 - 1.0*j03*j12*j22*j40*p00 + 1.0*j03*j12*j22*j41*p01;
j3->SetS(j0.S()|j1.S()|j2.S()|j4.S());
return j3;

}

// if outgoing index is 4
if (p_v->V()->id.back()==4){
const CVec4 <SType> & j0 = *(jj[0]->Get< CVec4 <SType> >());
const SComplex & j00 = j0[0];
const SComplex & j01 = j0[ATOOLS::Spinor<SType>::R1()];
const SComplex & j02 = j0[ATOOLS::Spinor<SType>::R2()];
const SComplex & j03 = j0[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p0 = p_v->J(0)->P();
const double& p00 = p0[0];
const double& p01 = p0[ATOOLS::Spinor<SType>::R1()];
const double& p02 = p0[ATOOLS::Spinor<SType>::R2()];
const double& p03 = p0[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j1 = *(jj[1]->Get< CVec4 <SType> >());
const SComplex & j10 = j1[0];
const SComplex & j11 = j1[ATOOLS::Spinor<SType>::R1()];
const SComplex & j12 = j1[ATOOLS::Spinor<SType>::R2()];
const SComplex & j13 = j1[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p1 = p_v->J(1)->P();
const double& p10 = p1[0];
const double& p11 = p1[ATOOLS::Spinor<SType>::R1()];
const double& p12 = p1[ATOOLS::Spinor<SType>::R2()];
const double& p13 = p1[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j2 = *(jj[2]->Get< CVec4 <SType> >());
const SComplex & j20 = j2[0];
const SComplex & j21 = j2[ATOOLS::Spinor<SType>::R1()];
const SComplex & j22 = j2[ATOOLS::Spinor<SType>::R2()];
const SComplex & j23 = j2[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p2 = p_v->J(2)->P();
const double& p20 = p2[0];
const double& p21 = p2[ATOOLS::Spinor<SType>::R1()];
const double& p22 = p2[ATOOLS::Spinor<SType>::R2()];
const double& p23 = p2[ATOOLS::Spinor<SType>::R3()];
const CVec4 <SType> & j3 = *(jj[3]->Get< CVec4 <SType> >());
const SComplex & j30 = j3[0];
const SComplex & j31 = j3[ATOOLS::Spinor<SType>::R1()];
const SComplex & j32 = j3[ATOOLS::Spinor<SType>::R2()];
const SComplex & j33 = j3[ATOOLS::Spinor<SType>::R3()];
const ATOOLS::Vec4D & p3 = p_v->J(3)->P();
const double& p30 = p3[0];
const double& p31 = p3[ATOOLS::Spinor<SType>::R1()];
const double& p32 = p3[ATOOLS::Spinor<SType>::R2()];
const double& p33 = p3[ATOOLS::Spinor<SType>::R3()];
ATOOLS::Vec4D p4 = -p0-p1-p2-p3;
const double& p40 = p4[0];
const double& p41 = p4[ATOOLS::Spinor<SType>::R1()];
const double& p42 = p4[ATOOLS::Spinor<SType>::R2()];
const double& p43 = p4[ATOOLS::Spinor<SType>::R3()];
CVec4<SType>* j4 = NULL;
j4 = CVec4<SType>::New();
(*j4)[0] = -1.0*j00*j11*j21*j32*p02 - 1.0*j00*j11*j21*j33*p03 + 1.0*j00*j11*j22*j32*p01 + 1.0*j00*j11*j23*j33*p01 + 1.0*j00*j12*j21*j31*p02 - 1.0*j00*j12*j22*j31*p01 - 1.0*j00*j12*j22*j33*p03 + 1.0*j00*j12*j23*j33*p02 + 1.0*j00*j13*j21*j31*p03 + 1.0*j00*j13*j22*j32*p03 - 1.0*j00*j13*j23*j31*p01 - 1.0*j00*j13*j23*j32*p02 + 1.0*j01*j10*j21*j32*p02 + 1.0*j01*j10*j21*j33*p03 - 1.0*j01*j10*j22*j31*p02 - 1.0*j01*j10*j23*j31*p03 + 1.0*j01*j11*j22*j30*p02 - 1.0*j01*j11*j22*j32*p00 + 1.0*j01*j11*j23*j30*p03 - 1.0*j01*j11*j23*j33*p00 - 1.0*j01*j12*j21*j30*p02 + 1.0*j01*j12*j22*j31*p00 - 1.0*j01*j13*j21*j30*p03 + 1.0*j01*j13*j23*j31*p00 - 1.0*j02*j10*j21*j32*p01 + 1.0*j02*j10*j22*j31*p01 + 1.0*j02*j10*j22*j33*p03 - 1.0*j02*j10*j23*j32*p03 + 1.0*j02*j11*j21*j32*p00 - 1.0*j02*j11*j22*j30*p01 + 1.0*j02*j12*j21*j30*p01 - 1.0*j02*j12*j21*j31*p00 + 1.0*j02*j12*j23*j30*p03 - 1.0*j02*j12*j23*j33*p00 - 1.0*j02*j13*j22*j30*p03 + 1.0*j02*j13*j23*j32*p00 - 1.0*j03*j10*j21*j33*p01 - 1.0*j03*j10*j22*j33*p02 + 1.0*j03*j10*j23*j31*p01 + 1.0*j03*j10*j23*j32*p02 + 1.0*j03*j11*j21*j33*p00 - 1.0*j03*j11*j23*j30*p01 + 1.0*j03*j12*j22*j33*p00 - 1.0*j03*j12*j23*j30*p02 + 1.0*j03*j13*j21*j30*p01 - 1.0*j03*j13*j21*j31*p00 + 1.0*j03*j13*j22*j30*p02 - 1.0*j03*j13*j22*j32*p00;
(*j4)[ATOOLS::Spinor<SType>::R1()] = -1.0*j00*j10*j22*j31*p02 + 1.0*j00*j10*j22*j32*p01 - 1.0*j00*j10*j23*j31*p03 + 1.0*j00*j10*j23*j33*p01 - 1.0*j00*j11*j20*j32*p02 - 1.0*j00*j11*j20*j33*p03 + 1.0*j00*j11*j22*j30*p02 + 1.0*j00*j11*j23*j30*p03 + 1.0*j00*j12*j20*j31*p02 - 1.0*j00*j12*j22*j30*p01 + 1.0*j00*j13*j20*j31*p03 - 1.0*j00*j13*j23*j30*p01 + 1.0*j01*j10*j20*j32*p02 + 1.0*j01*j10*j20*j33*p03 - 1.0*j01*j10*j22*j32*p00 - 1.0*j01*j10*j23*j33*p00 - 1.0*j01*j12*j20*j30*p02 + 1.0*j01*j12*j22*j30*p00 - 1.0*j01*j12*j22*j33*p03 + 1.0*j01*j12*j23*j33*p02 - 1.0*j01*j13*j20*j30*p03 + 1.0*j01*j13*j22*j32*p03 + 1.0*j01*j13*j23*j30*p00 - 1.0*j01*j13*j23*j32*p02 - 1.0*j02*j10*j20*j32*p01 + 1.0*j02*j10*j22*j31*p00 + 1.0*j02*j11*j20*j32*p00 - 1.0*j02*j11*j22*j30*p00 + 1.0*j02*j11*j22*j33*p03 - 1.0*j02*j11*j23*j32*p03 + 1.0*j02*j12*j20*j30*p01 - 1.0*j02*j12*j20*j31*p00 + 1.0*j02*j12*j23*j31*p03 - 1.0*j02*j12*j23*j33*p01 - 1.0*j02*j13*j22*j31*p03 + 1.0*j02*j13*j23*j32*p01 - 1.0*j03*j10*j20*j33*p01 + 1.0*j03*j10*j23*j31*p00 + 1.0*j03*j11*j20*j33*p00 - 1.0*j03*j11*j22*j33*p02 - 1.0*j03*j11*j23*j30*p00 + 1.0*j03*j11*j23*j32*p02 + 1.0*j03*j12*j22*j33*p01 - 1.0*j03*j12*j23*j31*p02 + 1.0*j03*j13*j20*j30*p01 - 1.0*j03*j13*j20*j31*p00 + 1.0*j03*j13*j22*j31*p02 - 1.0*j03*j13*j22*j32*p01;
(*j4)[ATOOLS::Spinor<SType>::R2()] = 1.0*j00*j10*j21*j31*p02 - 1.0*j00*j10*j21*j32*p01 - 1.0*j00*j10*j23*j32*p03 + 1.0*j00*j10*j23*j33*p02 + 1.0*j00*j11*j20*j32*p01 - 1.0*j00*j11*j21*j30*p02 - 1.0*j00*j12*j20*j31*p01 - 1.0*j00*j12*j20*j33*p03 + 1.0*j00*j12*j21*j30*p01 + 1.0*j00*j12*j23*j30*p03 + 1.0*j00*j13*j20*j32*p03 - 1.0*j00*j13*j23*j30*p02 - 1.0*j01*j10*j20*j31*p02 + 1.0*j01*j10*j21*j32*p00 + 1.0*j01*j11*j20*j30*p02 - 1.0*j01*j11*j20*j32*p00 + 1.0*j01*j11*j23*j32*p03 - 1.0*j01*j11*j23*j33*p02 + 1.0*j01*j12*j20*j31*p00 - 1.0*j01*j12*j21*j30*p00 + 1.0*j01*j12*j21*j33*p03 - 1.0*j01*j12*j23*j31*p03 - 1.0*j01*j13*j21*j32*p03 + 1.0*j01*j13*j23*j31*p02 + 1.0*j02*j10*j20*j31*p01 + 1.0*j02*j10*j20*j33*p03 - 1.0*j02*j10*j21*j31*p00 - 1.0*j02*j10*j23*j33*p00 - 1.0*j02*j11*j20*j30*p01 + 1.0*j02*j11*j21*j30*p00 - 1.0*j02*j11*j21*j33*p03 + 1.0*j02*j11*j23*j33*p01 - 1.0*j02*j13*j20*j30*p03 + 1.0*j02*j13*j21*j31*p03 + 1.0*j02*j13*j23*j30*p00 - 1.0*j02*j13*j23*j31*p01 - 1.0*j03*j10*j20*j33*p02 + 1.0*j03*j10*j23*j32*p00 + 1.0*j03*j11*j21*j33*p02 - 1.0*j03*j11*j23*j32*p01 + 1.0*j03*j12*j20*j33*p00 - 1.0*j03*j12*j21*j33*p01 - 1.0*j03*j12*j23*j30*p00 + 1.0*j03*j12*j23*j31*p01 + 1.0*j03*j13*j20*j30*p02 - 1.0*j03*j13*j20*j32*p00 - 1.0*j03*j13*j21*j31*p02 + 1.0*j03*j13*j21*j32*p01;
(*j4)[ATOOLS::Spinor<SType>::R3()] = 1.0*j00*j10*j21*j31*p03 - 1.0*j00*j10*j21*j33*p01 + 1.0*j00*j10*j22*j32*p03 - 1.0*j00*j10*j22*j33*p02 + 1.0*j00*j11*j20*j33*p01 - 1.0*j00*j11*j21*j30*p03 + 1.0*j00*j12*j20*j33*p02 - 1.0*j00*j12*j22*j30*p03 - 1.0*j00*j13*j20*j31*p01 - 1.0*j00*j13*j20*j32*p02 + 1.0*j00*j13*j21*j30*p01 + 1.0*j00*j13*j22*j30*p02 - 1.0*j01*j10*j20*j31*p03 + 1.0*j01*j10*j21*j33*p00 + 1.0*j01*j11*j20*j30*p03 - 1.0*j01*j11*j20*j33*p00 - 1.0*j01*j11*j22*j32*p03 + 1.0*j01*j11*j22*j33*p02 - 1.0*j01*j12*j21*j33*p02 + 1.0*j01*j12*j22*j31*p03 + 1.0*j01*j13*j20*j31*p00 - 1.0*j01*j13*j21*j30*p00 + 1.0*j01*j13*j21*j32*p02 - 1.0*j01*j13*j22*j31*p02 - 1.0*j02*j10*j20*j32*p03 + 1.0*j02*j10*j22*j33*p00 + 1.0*j02*j11*j21*j32*p03 - 1.0*j02*j11*j22*j33*p01 + 1.0*j02*j12*j20*j30*p03 - 1.0*j02*j12*j20*j33*p00 - 1.0*j02*j12*j21*j31*p03 + 1.0*j02*j12*j21*j33*p01 + 1.0*j02*j13*j20*j32*p00 - 1.0*j02*j13*j21*j32*p01 - 1.0*j02*j13*j22*j30*p00 + 1.0*j02*j13*j22*j31*p01 + 1.0*j03*j10*j20*j31*p01 + 1.0*j03*j10*j20*j32*p02 - 1.0*j03*j10*j21*j31*p00 - 1.0*j03*j10*j22*j32*p00 - 1.0*j03*j11*j20*j30*p01 + 1.0*j03*j11*j21*j30*p00 - 1.0*j03*j11*j21*j32*p02 + 1.0*j03*j11*j22*j32*p01 - 1.0*j03*j12*j20*j30*p02 + 1.0*j03*j12*j21*j31*p02 + 1.0*j03*j12*j22*j30*p00 - 1.0*j03*j12*j22*j31*p01;
j4->SetS(j0.S()|j1.S()|j2.S()|j3.S());
return j4;

}

      THROW(fatal_error, "Internal error in Lorentz calculator");
      return NULL;
    }

  };// end of class VVVVV4_Calculator

  template class VVVVV4_Calculator<double>;

}// end of namespace METOOLS


using namespace METOOLS;

DECLARE_GETTER(VVVVV4_Calculator<double>,"DVVVVV4",
	       Lorentz_Calculator,Vertex_Key);
Lorentz_Calculator *ATOOLS::Getter
<Lorentz_Calculator,Vertex_Key,VVVVV4_Calculator<double> >::
operator()(const Vertex_Key &key) const
{ return new VVVVV4_Calculator<double>(key); }

void ATOOLS::Getter<Lorentz_Calculator,Vertex_Key,
		    VVVVV4_Calculator<double> >::
PrintInfo(std::ostream &str,const size_t width) const
{ str<<"VVVVV4 vertex"; }
