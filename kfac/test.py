#!/usr/bin/python2

from mpi4py import MPI
from sys import path

# Generate one event, return a string conaining:
# event_weight*kfac followed by momenta of signal process
def event(generator, apply_kfac=False):
    Generator.GenerateOneEvent()
    signal_blob=Generator.GetBlobList().GetFirst(1)
    kfac=1.0
    ret=""
    for j in range(signal_blob.NInP()):
        part=signal_blob.InPart(j)
        fl=part.Flav()
        kfac+= (-1.0 if fl.IsAnti() else 1.0)*fl.Kfcode()
        ret+=" "+part.Momentum().__str__()
    for j in range(signal_blob.NOutP()):
        part=signal_blob.OutPart(j)
        fl=part.Flav()
        kfac+= (-1.0 if fl.IsAnti() else 1.0)*fl.Kfcode()
        ret+=" "+part.Momentum().__str__()
    if not apply_kfac: kfac = 1.0
    ret="{0:1.9e}".format(signal_blob["Weight"]*kfac)+ret
    return ret

if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("--mode", choices=["r","w"], default="w")
    parser.add_argument("--sherpa-path", help="Path to the Sherpa installation to be tested")
    parser.add_argument("--sherpa-args", nargs='+', type=str)
    args = parser.parse_args()
    sherpa_path = args.sherpa_path
    sherpa_args = ["Sherpa"] + (args.sherpa_args if args.sherpa_args!=None else [])

    # Get the python library installation path and make Sherpa's
    # python module available
    from subprocess import Popen, PIPE
    sherpa_python_path = Popen([sherpa_path+"/bin/Sherpa-config","--python-libs"], stdout=PIPE).communicate()[0].strip()
    path.append(sherpa_python_path)
    import Sherpa

    # Write mode: generate events with C++ K-Factor applied, write
    # weight and momenta to file. Read mode: generate events, apply
    # KFactor here manually and compare to weghts in file
    mode=args.mode
    if mode=="w":
        sherpa_args.append("KFACTOR=FLAV")
    if mode=="r":
        sherpa_args.append("KFACTOR=")

    # Initialize Sherpa
    Generator=Sherpa.Sherpa()
    Generator.InitializeTheRun(len(sherpa_args),sherpa_args)
    Generator.InitializeTheEventHandler()

    # Generate events
    with open('weights.dat',mode) as iofile:
        
        # Use the C++ K-Factor implementation, generate some events
        # and write the weights to file
        if(mode=="w"):
            for n in range(1,1+Generator.NumberOfEvents()):
                iofile.write(event(Generator, False)+"\n")
                
        # Use the kfactor implementation in the 'event' function above
        # and check if we recover the previous result
        if(mode=="r"):
            for line in iofile:
                line=line.strip(); evt=event(Generator, True).strip()
                if not evt==line: raise RuntimeError("Test not passed, compare\n{0}\n vs.\n{1}".format(line.strip(),evt.strip()))
                
    Generator.SummarizeRun()
    exit(0)
