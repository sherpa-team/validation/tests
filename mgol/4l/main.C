#include "SHERPA/Main/Sherpa.H"
#include "ATOOLS/Org/CXXFLAGS.H"
#include "ATOOLS/Org/CXXFLAGS_PACKAGES.H"
#include "ATOOLS/Org/Exception.H"
#include "ATOOLS/Org/Message.H"
#include "ATOOLS/Org/My_MPI.H"
#include "ATOOLS/Math/Vec4.H"

#include "MODEL/Main/Running_AlphaS.H"
#include "MODEL/Main/Running_AlphaQED.H"
#include "MODEL/Main/Coupling_Data.H"

#include "PHASIC++/Main/Phase_Space_Handler.H"
#include "PHASIC++/Process/External_ME_Args.H"
#include "PHASIC++/Process/Color_Correlated_ME2.H"
#include "PHASIC++/Process/Spin_Color_Correlated_ME2.H"

#include <assert.h>

using namespace ATOOLS;
using namespace PHASIC;

class MS : public Mass_Selector {
public:
  MS()  {};
  ~MS() {};
  double Mass(const Flavour& fl) const { return fl.Mass(); }
};

int main(int argc,char* argv[])
{

#ifdef USING__MPI
  MPI::Init(argc,argv);
#endif

  SHERPA::Sherpa *Generator(new SHERPA::Sherpa());
  Generator->InitializeTheRun(argc,argv); MS ms;
      
  // Flavours
  const Flavour_Vector inflavs  = {Flavour(21),Flavour(21)};
  const Flavour_Vector outflavs = {Flavour(11),Flavour(-11),
				   Flavour(13),Flavour(-13)};
  Flavour_Vector flavs = inflavs; 
  flavs.insert(flavs.end(),outflavs.begin(),outflavs.end());

  const size_t nin  = inflavs .size();
  const size_t nout = outflavs.size();

  External_ME_Args olargs(inflavs, outflavs, std::vector<double>(), "OpenLoops");
  External_ME_Args mgargs(inflavs, outflavs, std::vector<double>(), "MadGraph");

  // Load correlated ME
  PHASIC::Color_Correlated_ME2* p_olme(PHASIC::Color_Correlated_ME2::GetME2(olargs));
  PHASIC::Color_Correlated_ME2* p_mgme(PHASIC::Color_Correlated_ME2::GetME2(mgargs));

  if(!p_olme) THROW(fatal_error, "Failed to load OL ME");
  if(!p_mgme) THROW(fatal_error, "Failed to load MG ME");

  // Set up running couplings
  MODEL::Coupling_Data* p_qcd = new MODEL::Coupling_Data(MODEL::as,  "Alpha_QCD", MODEL::as->Default());
  MODEL::Coupling_Data* p_qed = new MODEL::Coupling_Data(MODEL::aqed,"Alpha_QED", MODEL::aqed->Default());
  double scale = pow(91.2,2); p_qcd->SetScale(&scale);
  MODEL::Coupling_Map cplmap;
  cplmap.insert(std::make_pair("Alpha_QCD", p_qcd));

  p_olme->SetCouplings(cplmap);
  p_mgme->SetCouplings(cplmap);

  std::cout << std::setprecision(10);

  for (int m(0); m<4; m++)
    {
      Vec4D_Vector p(inflavs.size()+outflavs.size());
      PHASIC::Phase_Space_Handler::TestPoint(&p[0],nin,nout,flavs, &ms);
      ATOOLS::Poincare cms=ATOOLS::Poincare(p[0]+p[1]);
      for (size_t i(0);i<p.size();++i) cms.Boost(p[i]);
      
      p_olme->Calc(p);
      p_mgme->Calc(p);

      double olme = p_olme->GetBorn2();
      double mgme = p_mgme->GetBorn2();
      double  err = std::abs((olme - mgme)/olme);
      std::cout << "|M|^2 = "
		<< olme << " vs " << mgme << " rel. err: " << err <<std::endl;

      for(int i(0); i<p.size(); i++)
	for(int j(0); j<p.size(); j++)
	  if(flavs[i].Strong() && flavs[j].Strong() && (i!=j))
	    {
	      double olme = p_olme->GetValue(i,j);
	      double mgme = p_mgme->GetValue(i,j);
	      double  err = std::abs((olme - mgme)/olme);

	      std::cout << "<M | T_" << i << " T_" << j << " | M> = "
			<< olme << " vs " << mgme << " rel. err: " << err <<std::endl;
	  
	      assert(err < 1.0e-2);
	    }
    }
  
  
  delete p_mgme;
  delete p_olme;
  delete Generator;
  
#ifdef USING__MPI
  MPI::Finalize();
#endif
  
  return 0;
  
}
