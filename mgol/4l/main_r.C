#include "SHERPA/Main/Sherpa.H"
#include "ATOOLS/Org/CXXFLAGS.H"
#include "ATOOLS/Org/CXXFLAGS_PACKAGES.H"
#include "ATOOLS/Org/Exception.H"
#include "ATOOLS/Org/Message.H"
#include "ATOOLS/Org/My_MPI.H"
#include "ATOOLS/Math/Vec4.H"

#include "MODEL/Main/Running_AlphaS.H"
#include "MODEL/Main/Running_AlphaQED.H"
#include "MODEL/Main/Coupling_Data.H"

#include "PHASIC++/Main/Phase_Space_Handler.H"
#include "PHASIC++/Process/External_ME_Args.H"
#include "PHASIC++/Process/Tree_ME2_Base.H"

#include <assert.h>
#include <chrono>

using namespace ATOOLS;
using namespace PHASIC;

class MS : public Mass_Selector {
public:
  MS()  {};
  ~MS() {};
  double Mass(const Flavour& fl) const { return fl.Mass(); }
};

int main(int argc,char* argv[])
{

#ifdef USING__MPI
  MPI::Init(argc,argv);
#endif

  SHERPA::Sherpa *Generator(new SHERPA::Sherpa());
  Generator->InitializeTheRun(argc,argv); MS ms;
      
  // Flavours
  const Flavour_Vector inflavs  = {Flavour(21),Flavour(21)};
  const Flavour_Vector outflavs = {Flavour(11),Flavour(-11),
				   Flavour(13),Flavour(-13),Flavour(21)};
  Flavour_Vector flavs = inflavs; 
  flavs.insert(flavs.end(),outflavs.begin(),outflavs.end());

  const size_t nin  = inflavs .size();
  const size_t nout = outflavs.size();

  External_ME_Args olargs(inflavs, outflavs, std::vector<double>(2,-1), "OpenLoops");
  External_ME_Args mgargs(inflavs, outflavs, std::vector<double>(2,-1), "MadGraph");

  // Load correlated ME
  PHASIC::Tree_ME2_Base* p_olme(PHASIC::Tree_ME2_Base::GetME2(olargs));
  PHASIC::Tree_ME2_Base* p_mgme(PHASIC::Tree_ME2_Base::GetME2(mgargs));

  if(!p_olme) THROW(fatal_error, "Failed to load OL ME");
  if(!p_mgme) THROW(fatal_error, "Failed to load MG ME");

  // Set up running couplings
  MODEL::Coupling_Data* p_qcd = new MODEL::Coupling_Data(MODEL::as,  "Alpha_QCD", MODEL::as->Default());
  MODEL::Coupling_Data* p_qed = new MODEL::Coupling_Data(MODEL::aqed,"Alpha_QED", MODEL::aqed->Default());
  double scale = pow(91.2,2); p_qcd->SetScale(&scale);
  MODEL::Coupling_Map cplmap;
  cplmap.insert(std::make_pair("Alpha_QCD", p_qcd));

  p_olme->SetCouplings(cplmap);
  p_mgme->SetCouplings(cplmap);

  std::cout << std::setprecision(10);
  const int numpoints(10);

  double dt_ol(0.0);
  double dt_mg(0.0);
  auto t0 = std::chrono::high_resolution_clock::now();
  auto t1 = std::chrono::high_resolution_clock::now();

  for (int m(0); m<numpoints; m++)
    {
      Vec4D_Vector p(inflavs.size()+outflavs.size());
      PHASIC::Phase_Space_Handler::TestPoint(&p[0],nin,nout,flavs, &ms);
      ATOOLS::Poincare cms=ATOOLS::Poincare(p[0]+p[1]);
      for (size_t i(0);i<p.size();++i) cms.Boost(p[i]);

      std::cout << "==============================================================" << std::endl;

      for (size_t i(0);i<p.size();++i) std::cout << "p(" << i << ")=" << (p[i]) << std::endl;
      
      t0 = std::chrono::high_resolution_clock::now();
      double olme = p_olme->Calc(p);
      t1 = std::chrono::high_resolution_clock::now();
      double dt_ol_ = std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count();
      dt_ol += dt_ol_;

      t0 = std::chrono::high_resolution_clock::now();
      double mgme = p_mgme->Calc(p);
      t1 = std::chrono::high_resolution_clock::now();
      double dt_mg_ = std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count();
      dt_mg += dt_mg_;

      double  err = std::abs((olme - mgme)/olme);
      std::cout << "|M|^2 = "
		<< olme << " (OL) vs " << mgme << " (MG) | rel. err: " << err << std::endl;
      std::cout << "Timing [s]: "
		<< dt_ol_ << " (OL) vs " << dt_mg_ << " (MG)" << std::endl;
    }

  std::cout << "Overall timing [s] : "
	    << dt_ol/1000. << " (OL) vs " <<  dt_mg/1000. << " (MG)" << std::endl;
  
  
  delete p_mgme;
  delete p_olme;
  delete Generator;
  
#ifdef USING__MPI
  MPI::Finalize();
#endif
  
  return 0;
  
}
