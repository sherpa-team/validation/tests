import sys, re

fileOL=open(sys.argv[1],'r')

fileOLloopinduced=open(sys.argv[2],'w')

#BornsOL=[]

MomlistOL=[]

for line in fileOL:
    momenta=""
    if line.find('Momenta')!=-1:
        momenta=line
	second=line.split()[2].split(',')
	second[0]=second[0][2:]	
	second[-1]=second[-1][:-2]
	third=[]
	for k in second:
	    if ('(' in k):
		k=k[1:]
	    if (')' in k):
		k=k[:-1]
	    third.append(k)
	MomlistOL.append(third)
#    if line.find('born is:')!=-1:
#        lines=line.split()
#        BornsOL.append(lines[-1])
    
n_flav = len(MomlistOL[0])/4
j=1

if not (len(MomlistOL[0])%4)==0:
    print "ERROR, components of momenta not a multiple of four!"
    fileOL.close()
    fileOLloopinduced.close()
    exit(1)
#if not (len(MomlistOL)==len(BornsOL)):
#    print "ERROR, different amount of momenta ({0}) and results ({1})!".format(len(MomlistOL), len(BornsOL))
#    fileOL.close()
#    fileOLloopinduced.close()
#    exit(1)
for mom in MomlistOL:
    if (mom==MomlistOL[0]):
	continue
    for i in range(n_flav):
    	fileOLloopinduced.write(str(mom[0+i*4])+' '+str(mom[1+i*4])+' '+str(mom[2+i*4])+' '+str(mom[3+i*4])+' ')
#    fileOLloopinduced.write(str(BornsOL[j])+'\n')
    fileOLloopinduced.write('\n')
    j+=1

fileOL.close()
fileOLloopinduced.close()

#if not j==10001:
if not j==201:
    print "{0} is not 200, files definitely not successfully created".format(j)
