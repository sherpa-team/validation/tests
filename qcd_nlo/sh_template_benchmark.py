#!/usr/bin/env python2
import sys
from math import sqrt
from os import path,getcwd
from mpi4py import MPI
sys.path.append('/work1/Stephan/programs/RecolaInterface-program/lib/python2.7/site-packages')
if len(sys.argv)<2: raise RuntimeError('Too few arguments given')
sys.argv.append('INIT_ONLY=2')
sys.argv.append('OUTPUT=0')

import Sherpa

n_flav = len(${in_flavs})+len(${out_flavs})

me_fname = sys.argv.pop(1)
cur_dir = getcwd()
benchmark_results = path.join(cur_dir, 'benchmark_results.dat')

Generator=Sherpa.Sherpa()
Generator.InitializeTheRun(len(sys.argv),sys.argv)
Process=Sherpa.MEProcess(Generator)

# Incoming flavors must be added first!
for fl in ${in_flavs}:
    Process.AddInFlav(fl);
for fl in ${out_flavs}:
    Process.AddOutFlav(fl);

Process.Initialize();

try:
    with open(me_fname,'r') as f:
        with open(benchmark_results,'w') as benchmark_file:
	    for line in f:
                numbers = line.strip().split()
                if not len(numbers)-1==4*n_flav:
		    if not len(numbers)==4*n_flav:
			raise RuntimeError("Number of momenta in file {0} doesn't match process.".format(me_fname))

                for i in range(n_flav):
                    Process.SetMomentum(i, float(numbers[0+i*4]), float(numbers[1+i*4]), float(numbers[2+i*4]), float(numbers[3+i*4]))
	   	    benchmark_file.write(str(numbers[0+i*4])+' '+str(numbers[1+i*4])+' '+str(numbers[2+i*4])+' '+str(numbers[3+i*4])+' ')
            
                s_me = Process.CSMatrixElement()
	        benchmark_file.write(str(s_me)+'\n')

except Sherpa.SherpaException as exc:
    print exc
    exit(1)

exit(0)
