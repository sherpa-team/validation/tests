import sys, re

fileOL=open(sys.argv[1],'r')

fileOLVirtual=open(sys.argv[2],'w')

FinitesOL=[]

MomlistOL=[]
Momlist=[]

for line in fileOL:
    momenta=""
    if line.find('Momenta')!=-1:
        momenta=line
	second=line.split()[2].split(',')
	second[0]=second[0][2:]	
	second[-1]=second[-1][:-2]
	third=[]
	for k in second:
	    if ('(' in k):
		k=k[1:]
	    if (')' in k):
		k=k[:-1]
	    third.append(k)
	MomlistOL.append(third)
#    if line.find('finite is:')!=-1:
#        lines=line.split()
#        FinitesOL.append(lines[-1])
#    if line.find('Born')!=-1:
#        lines=line.split()
#        BornsOL.append(lines[-1])

n_flav = len(MomlistOL[0])/4
j=0

if not (len(MomlistOL[0])%4)==0:
    print "ERROR, components of momenta not a multiple of four!"
    fileOL.close()
    fileOLVirtual.close()
    exit(1)
#if not (len(MomlistOL)==len(FinitesOL)):
#    print "ERROR, different amount of momenta ({0}) and results ({1})!".format(len(MomlistOL), len(FinitesOL))
#    fileOL.close()
#    fileOLVirtual.close()
#    exit(1)
for mom in MomlistOL:
    for i in range(n_flav):
    	fileOLVirtual.write(str(mom[0+i*4])+' '+str(mom[1+i*4])+' '+str(mom[2+i*4])+' '+str(mom[3+i*4])+' ')
#    fileOLVirtual.write(str(FinitesOL[j])+'\n')
    fileOLVirtual.write('\n')
    j+=1

fileOL.close()
fileOLVirtual.close()

if not j==200:
    print "{0} is not 200, files definitely not successfully created".format(j)
