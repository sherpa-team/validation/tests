#!/usr/bin/env python2

import sys, re
from math import sqrt

#print str(sys.argv[1])
#fileOutput=open(str(sys.argv[1]),'r')

#fileBorn=open(str(sys.argv[2]),'a')
#fileOverview=open(str(sys.argv[3]),'a')

devs = []
failed = 0
passed = 0
counter = 0
process_counter = 0
process_strings = []
process = 0
process_string = ""

print "Starting born tests..."

for symbol in sys.argv[4]:
    if symbol == "[":
	continue
    if symbol == "]":
	process_strings.append(process_string)
	continue
    if symbol == "'":
	continue
    if symbol == ",":
	process_strings.append(process_string)
	process_string=""
	process+=1
	continue
    process_string+=symbol
threshold = float(sys.argv[5])
set_ratio = float(sys.argv[6])

with open(sys.argv[1],'r') as f:
    with open(sys.argv[2],'a') as bf:
	with open(sys.argv[3],'a') as O:
    	    O.write("{0:45}".format('\nBorn Test for '+process_strings[process_counter]))

        bf.write('\n--------------------------------------------------------\n')
	bf.write('Begin test for '+process_strings[process_counter]+'\n')
	bf.write('Deviations larger '+str(threshold)+':\n')
	for line in f:
            if line.find('ratio:')!=-1:
 		counter+=1
		lines=line.split()
	        devs.append(abs(float(1)-float(lines[-1])))
  	        if (abs(float(1)-float(lines[-1])) > threshold):
		    bf.write('Momentum ID: '+str(counter)+', dev: '+str(abs(1.0-float(lines[-1])))+'\n')
		    failed+=1
	        else:
		    passed+=1

		if (counter==200):
		    if failed==0:
	    		bf.write('...none detected\n')

		    max_dev = max(devs)
		    min_dev = min(devs)
		    spread  = sqrt(sum([de*de for de in devs])/len(devs))
		    average = sum([abs(de) for de in devs])/len(devs)
		    ratio   = float(failed)/float(len(devs))
		    success = ratio<set_ratio

		    msg = "Test for {0} {1}passed (ratio {2} {3}): \n".format(process_strings[process_counter],'' if success else 'not ','<' if success else '>', set_ratio)
		    msg += "{0:45} {1: 1.3e}\n".format('Expected deviation', threshold)
		    msg += "{0:45} {1: 1.3e}\n".format('Maximum deviation', max_dev)
		    msg += "{0:45} {1: 1.3e}\n".format('Minimum deviation', min_dev)
	   	    msg += "{0:45} {1: 1.3e}\n".format('RMS deviation', spread)
		    msg += "{0:45} {1: 1.3e}\n".format('Average abs. deviation', average)
		    msg += "{0:45} {1: 1.5f}\n".format('Ratio failed', ratio)
		    msg += "--------------------------------------------------------\n"
		    bf.write(msg)

		    with open(sys.argv[3],'a') as O:
		    	O.write(" {0}passed".format('' if success else 'not ' ))
			process_counter+=1
		    	if process_counter < (len(process_strings)):
      	    		    O.write("{0:45}".format('\nBorn Test for '+process_strings[process_counter]))
			    bf.write('\n--------------------------------------------------------\n')
		 	    bf.write('Begin test for '+process_strings[process_counter]+'\n')
			    bf.write('Deviations larger '+str(threshold)+':\n')
		        #print "process_counter = ", process_counter, " counter = ", counter
	

		    devs = []
		    failed = 0
		    passed = 0
		    counter= 0

print "Born tests done!"
exit(0)
