#!/usr/bin/env python2

from os import path,makedirs,getcwd,chdir
from shutil import copyfile,copy
from subprocess import check_call,call
from glob import glob
from datetime import datetime

from test_processes import all_test_procs as procs
from templates import sh_template,sh_template_benchmark,run_template

def write_run_card(proc,loopgen):
    dir_path = path.join(proc.test_dir_name(),'Run.dat')
    proc_string  = "Process "
    proc_string += " ".join([str(ide) for ide in proc._in_ids])
    proc_string += " -> "
    proc_string += " ".join([str(ide) for ide in proc._out_ids])
    external_t = 1 if 6 in proc._out_ids or -6 in proc._out_ids or 6 in proc._in_ids or -6 in proc._in_ids else 0
    external_ew = 1 if 23 in proc._out_ids or 24 in proc._out_ids or -24 in proc._out_ids else 0
    external_h = 1 if 25 in proc._out_ids else 0
    width_6="" if external_t==1 else "%"
    width_ew="" if external_ew==1 else "%"
    width_25="" if external_h==1 else "%"
    is_nlo = 1 if (proc._qcd_me_part=="B") or (proc._qcd_me_part=="V") else 0
    is_loop_induced = 1 if (proc._qcd_me_part=="LI") else 0
    is_nlo_calculation="" if is_nlo==1 else "%"
    is_loop_induced_calculation="" if is_loop_induced==1 else "%"
 
    with open(dir_path,'w') as f:
        f.write(run_template.substitute(loop_generator=loopgen,
					width_zero_6=width_6,
					width_zero_ew=width_ew,
					width_zero_25=width_25,
					nlo_calculation=is_nlo_calculation,
					loop_induced=is_loop_induced_calculation,
					en=(proc._cms/2.0),
                                        in_flav0=2212, #proc._in_ids[0],
                                        in_flav1=2212, #proc._in_ids[1],
                                        process_string=proc_string,
					qcd_order=proc._qcd_order,
					ew_order=proc._ew_order,
					qcd_me_part=proc._qcd_me_part))

def write_sh_script(proc,ratio):
    f_path = path.join(proc.test_dir_name(),'run_sh.py')
    with open(f_path,'w') as f:
        f.write(sh_template.substitute(in_flavs=proc._in_ids,
                                       out_flavs=proc._out_ids,
                                       threshold=proc._threshold,
                                       procstring=str(proc),
				       set_ratio=ratio))

def write_benchmark_sh_script(proc):
    f_path = path.join(proc.test_dir_name(),'run_benchmark_sh.py')
    with open(f_path,'w') as f:
       f.write(sh_template_benchmark.substitute(in_flavs=proc._in_ids,
                                       out_flavs=proc._out_ids,
                                       threshold=proc._threshold,
                                       procstring=str(proc)))

def create_benchmark_file(proc):
    cur_dir = getcwd()
    chdir(proc.test_dir_name())
    try:
	with open(path.join(cur_dir,proc.test_dir_name(),'benchmark_results.dat')) as f: 
	    print proc.test_dir_name()+"\nBenchmark file exists\n"
    except IOError:
    	print proc.test_dir_name()+"\nBenchmark file does not exists\n"
    	check_call(['chmod','+x','./run_benchmark_sh.py'])
    	call(['./run_benchmark_sh.py','set_of_points.dat'])
    	try:
	    check_call(['./makelibs'])
	    check_call(['./run_benchmark_sh.py','set_of_points.dat'])
	    print "DONE!\n"
    	except OSError:
	    print " - no libraries needed - DONE!\n"
    #if proc._qcd_me_part=="B":
   # 	try:
   #         with open(path.join(cur_dir,proc.test_dir_name(),'ol_results_B.dat')) as f: 
   #             print proc.test_dir_name()+"\nOpenLoops born file exists\n"
   # 	except IOError:
#	    print proc.test_dir_name()+"\nOpenLoops born file does not exists\n"
#	    check_call(['chmod','+x','./run_ol_born_sh.py'])
#    	    call(['./run_ol_born_sh.py','set_of_points.dat'])
#	    try:
		#check_call(['./makelibs'])
		#check_call(['./run_ol_born_sh.py','set_of_points.dat'])
#		print "DONE!\n"
#	    except OSError:
#		print " - no libraries needed - DONE!\n"
 #   elif proc._qcd_me_part=="V":
  #  	try:
  #          with open(path.join(cur_dir,proc.test_dir_name(),'ol_results_V.dat')) as f: 
  #              print proc.test_dir_name()+"\nOpenLoops virtual file exists\n"
  #  	except IOError:
#	    print proc.test_dir_name()+"\nOpenLoops virtual file does not exists\n"
#	    check_call(['chmod','+x','./run_ol_virtual_sh.py'])
 #   	    call(['./run_ol_virtual_sh.py','set_of_points.dat'])
#	    try:
#		#check_call(['./makelibs'])
#		#check_call(['./run_ol_virtual_sh.py','set_of_points.dat'])
#		print "DONE!\n"
#	    except OSError:
#		print " - no libraries needed - DONE!\n"
 #   else:
#	print "Error in process "+str(proc.test_dir_name())+"\nNeed either Virtual (V) or Born (B) part as input."
#        exit(1)
    chdir(cur_dir)
    
def call_sh_script(proc):
    cur_dir = getcwd()
    chdir(proc.test_dir_name())
    with open(path.join(cur_dir,'results_'+str(proc._qcd_me_part)+'.txt'),'a') as f:
	f.write('\n--------------------------------------------------------\n')	
	f.write('Begin test for '+proc.test_dir_name()+'\n')
    with open(path.join(cur_dir,'results_overview.txt'),'a') as f:
	f.write("{0:45}".format('\nTest for '+proc.test_dir_name()))
    check_call(['chmod','+x','./run_sh.py'])
    check_call(['./run_sh.py','benchmark_results.dat','../results_'+str(proc._qcd_me_part)+'.txt',
									'../results_overview.txt',proc.test_dir_name()+'.pdf'])
    #try:
        #with open(path.join(cur_dir,proc.test_dir_name(),'distribution_neg.yoda')) as f: 
            #print 'Creating distribution of the deviation for {0}'.format(proc.test_dir_name())
	    #check_call(['yoda2flat','distribution_neg.yoda','distribution_pos.yoda'])
 	    #check_call(['../distribution.py',proc.test_dir_name(),'distribution_neg.dat','distribution_pos.dat'])
    	    #check_call(['make-plots',proc.test_dir_name()+'.dat'])
  	    #check_call(['mv',proc.test_dir_name()+'.pdf','../distributions/'])
 	    #check_call(['rm','-f','distribution_neg.dat','distribution_pos.dat'])
    #except IOError:
	#pass
    chdir(cur_dir)

def born_tests(procs, threshold, ratio):
    process_string=[]
    for proc in procs:
	if proc._qcd_me_part!="V":
	    continue
  	process_string.append(str(proc))
    check_call(['./sh_born.py','output.txt','results_B.txt','results_overview.txt',str(process_string),str(threshold),str(ratio)])

def clear(proc):
    cur_dir = getcwd()
    chdir(proc.test_dir_name())
    check_call(['rm','-rf','makelibs','param_card.dat','benchmark_results.dat','Process/','ol_results_B.dat','output_cll',
						'ol_results_V.dat','Run.dat','run_ol_born_sh.py','run_ol_sh.py','run_benchmark_sh.py',
						'run_sh.py','SConstruct','Sherpa_References.tex','run_ol_virtual_sh.py','.sconsign.dblite','table.txt',
						'distribution_neg.yoda','distribution_pos.yoda',proc.test_dir_name()+'.dat','table.txt'])
    print "Directory "+str(proc.test_dir_name())+":"
    check_call(['ls'])
    chdir(cur_dir)

def temp(proc):
    cur_dir = getcwd()
    chdir(proc.test_dir_name())
    print "Directory "+str(proc.test_dir_name())+":"
    #check_call(['svn','rm','--force','.sconsign.dblite'])
    check_call(['rm','-f','benchmark_results.dat'])
    #check_call(['mv','benchmark_results_var_scale.dat','benchmark_results.dat'])
    with open(path.join(cur_dir,proc.test_dir_name()+'/set_of_points.dat'),'r') as f:
        k=0
	for line in f:
	    k+=1
	print k
    #check_call(['rm','-f','set_of_points.dat'])
    #check_call(['ls','-R','Process/Amegic'])
    #check_call(['rm','-rf','makelibs','Process'])
    #call(['/work1/Stephan/programs/RecolaInterface-program/bin/Sherpa','-e0','-g','OUTPUT=0'])
    #call(['./makelibs'])
    chdir(cur_dir)
    
if __name__=="__main__":

    #for now: set variable manually
    run_mode = "TESTING" 
    ratio = 0.05
    
    if run_mode=="INIT":
	print "\n Creating OpenLoops results from the set_of_points.dat files \n"
	for proc in procs:
            write_run_card(proc,"OpenLoops")
	    write_benchmark_sh_script(proc)        
  	    create_benchmark_file(proc)
    elif run_mode=="CLEAR":
	for proc in procs:
	    clear(proc)
	print "Finished Cleaning of directories"
    elif run_mode=="TEMP":
	for proc in procs:
	    #write_run_card(proc,"OpenLoops")
	    temp(proc)
	#born_tests(procs,1e-08,ratio)
    elif run_mode=="TESTING":
	print "\n Starting tests\n"
	print datetime.now().strftime("%A, %d. %B %Y %I:%M%p")
	current_directory = getcwd()
	check_call(['rm','-rf','results_V.txt','results_B.txt','results_LI.txt','results_overview.txt'])
	with open(path.join(current_directory,'results_overview.txt'),'w') as f:
	    f.write('Created overview file for virtual and born tests.\n')
	    f.write(datetime.now().strftime("%A, %d. %B %Y %I:%M%p")+'\n')
	    f.write('The test is passed if the ratio of phase space points larger then the threshold is less then '+str(ratio)+'.\nThe threshold is 1.0e-6 for loop-induced processes and 1.0e-8 for the other processes.')
	with open(path.join(current_directory,'results_V.txt'),'w') as f:
	    f.write('Created file for virtual tests.\n')
	    f.write(datetime.now().strftime("%A, %d. %B %Y %I:%M%p")+'\n')
	with open(path.join(current_directory,'results_B.txt'),'w') as f:
	    f.write('Created file for born tests.\n')
	    f.write(datetime.now().strftime("%A, %d. %B %Y %I:%M%p")+'\n')
	with open(path.join(current_directory,'results_LI.txt'),'w') as f:
	    f.write('Created file for loop-induced tests.\n')
	    f.write(datetime.now().strftime("%A, %d. %B %Y %I:%M%p")+'\n')
	for proc in procs:
            write_run_card(proc,"Recola")
	    write_sh_script(proc,ratio)
    	    call_sh_script(proc)
        born_tests(procs,1e-08,ratio)
    else:
	print "Unknown run mode"
    
    exit(0)
    
