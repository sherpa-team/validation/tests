#!/usr/bin/env python2
import sys
import yoda
import numpy as np
from math import sqrt
from mpi4py import MPI
import matplotlib.pyplot as plt
from subprocess import check_call,call

sys.path.append('/work1/Stephan/programs/RecolaInterface-program/lib/python2.7/site-packages')
if len(sys.argv)<5: raise RuntimeError('Too few arguments given')
sys.argv.append('INIT_ONLY=2')
sys.argv.append('OUTPUT=0')

import Sherpa

n_flav = len(${in_flavs})+len(${out_flavs})

benchmark_results = sys.argv.pop(1)
results_file = sys.argv.pop(1)
results_overview = sys.argv.pop(1)
figure_output= sys.argv.pop(1)
temporary_output="temp.txt"
devs = []
abs_devs = []
failed = 0
passed = 0
bins_pos = yoda.core.logspace(10,1e-10,1e-5)
#bins_full = []
#for k in xrange(21):
#    bins_full.append(1.0-bins_pos[20-k])
#bins_full.append(1.0)
#bins_full += *bins_pos
#bins_neg = []
#for k in range(21):
#    bins_neg.append(-bins_pos[20-k])
#bins_full = bins_neg + bins_pos

#h1 = yoda.Histo1D(bins_pos, '/distribution_pos')
#h2 = yoda.Histo1D(bins_pos, '/distribution_neg') 

Generator=Sherpa.Sherpa()
Generator.InitializeTheRun(len(sys.argv),sys.argv)
Process=Sherpa.MEProcess(Generator)

# Incoming flavors must be added first!
for fl in ${in_flavs}:
    Process.AddInFlav(fl);
for fl in ${out_flavs}:
    Process.AddOutFlav(fl);

Process.Initialize();

with open(benchmark_results,'r') as f:
    with open(results_file,'a') as rf:
	rf.write('Deviations larger '+str(${threshold})+':\n')
	for line in f:
            numbers = line.strip().split()
            if not len(numbers)-1==4*n_flav:
        	raise RuntimeError("Number of momenta in in file {0} doesn't match process.".format(benchmark_results))

            for i in range(n_flav):
                Process.SetMomentum(i, float(numbers[0+i*4]), float(numbers[1+i*4]), float(numbers[2+i*4]), float(numbers[3+i*4]))
            
            f_me = float(numbers[-1])
	    s_me = Process.CSMatrixElement()
	    if f_me == 0.0: 
		f_me = 1e-90
            if abs(s_me)<1e-90:
		    s_me = 1e-90 
	    cur_dev = (f_me-s_me)/(f_me)
	    devs.append(cur_dev)
	    cur_abs_dev = abs((f_me-s_me)/f_me)
	    abs_devs.append(cur_abs_dev)
#####
#	    if cur_dev > 0:
#		h1.fill(cur_dev,1.0)
#	    else:
#		h2.fill(-cur_dev,1.0)

#####	
	    if abs((f_me-s_me)/(f_me))>${threshold}:
		for i in range(n_flav):
		    rf.write(str(numbers[0+i*4])+' '+str(numbers[1+i*4])+' '+str(numbers[2+i*4])+' '+str(numbers[3+i*4])+' ')
	        rf.write('OL: '+str(f_me)+', R: '+str(s_me)+', dev: '+str((f_me-s_me)/(f_me))+'\n')
		failed+=1
	    else:
		passed+=1
	if failed==0:
	    rf.write('...none detected\n')

max_dev = max(devs)
min_dev = min(devs)
spread  = sqrt(sum([de*de for de in devs])/len(devs))
average = sum([abs(de) for de in devs])/len(devs)
ratio   = float(failed)/float(len(devs))
success = ratio<${set_ratio}

#h1.normalize(normto=h1.numEntries()) #
#h2.normalize(normto=h2.numEntries()) #

msg = "Test for ${procstring} {0}passed (ratio {1} ${set_ratio}): \n".format('' if success else 'not ','<' if success else '>')
msg += "{0:45} {1: 1.3e}\n".format('Expected deviation', ${threshold})
msg += "{0:45} {1: 1.3e}\n".format('Maximum deviation', max_dev)
msg += "{0:45} {1: 1.3e}\n".format('Minimum deviation', min_dev)
msg += "{0:45} {1: 1.3e}\n".format('RMS deviation', spread)
msg += "{0:45} {1: 1.3e}\n".format('Average abs. deviation', average)
msg += "{0:45} {1: 1.5f}\n".format('Ratio failed', ratio)
msg += "--------------------------------------------------------\n"

with open(results_file,'a') as rf:
    rf.write(msg)	
with open(results_overview,'a') as f:
    f.write(" {0}passed".format('' if success else 'not ' ))

if not success:
    print '\n--------------------------------------------------------\n'+msg
    exit(0)
    #raise Sherpa.Exception(msg)
    
else:
    print '\n--------------------------------------------------------\n'+msg
    exit(0)


######################################
###move up to histogram the process###
with open(temporary_output,'w') as tf:
    for k in xrange(len(abs_devs)):
        if abs_devs[k] < 1e-10:
            abs_devs[k]=1.01e-10
	tf.write(str(abs_devs[k])+'\n')

#plt.hist(abs_devs, bins=bins_pos, histtype='step', color='red', label='loop-induced gg->ZZ')
plt.hist(abs_devs, bins=bins_pos, histtype='step', color='red', label='Deviations between OpenLoops and Recola')
#plt.hist(np.negative(abs_devs), bins=bins_full, histtype='step', color='red', label='neg. Deviations')
y, bin_edges = np.histogram(abs_devs, bins=bins_pos)
bin_centers = np.sqrt(bin_edges[:-1] * bin_edges[1:])
plt.errorbar(
    bin_centers,
    y,
    yerr = y**0.5,
    marker = '.',
    drawstyle = 'steps-mid-',
    ecolor='red',
    fmt='none'
)
#y, bin_edges = np.histogram(np.negative(abs_devs), bins=bins_full)
#plt.errorbar(
    #bin_centers,
    #y,
    #yerr = y**0.5,
    #marker = '.',
    #drawstyle = 'steps-mid-',
    #ecolor='red',
    #fmt='none'
#)

plt.title(figure_output)
#plt.title('Distribution of the deviations for loop-induced process gg->ZZ')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Deviations = |(Recola - OpenLoops)/OpenLoops|')
plt.ylabel('Number of events')
plt.grid(True)
plt.legend()
#plt.title('test2')
#plt.xscale('log')
#plt.grid(True)
plt.savefig(figure_output)
plt.close()
check_call(['mv',figure_output,'../distributions/'])
check_call(['mv','./temp.txt','../distributions/'+str(figure_output)+'.txt'])
#   yoda.write(h1, 'distribution_pos.yoda')
#   yoda.write(h2, 'distribution_neg.yoda')
    
