for i in '21_21_23_23'
#pc-atlas: '21_21_25_23' '21_21_22_22' '21_21_24_-24' '21_21_25_23_21' '21_21_25_25' '21_21_25_25_21'  '21_21_13_-13' '21_21_11_-11_22' '21_21_12_-12_22_21' '21_21_13_13_-13_-13' '21_21_22_21' '21_21_22_21_21' '21_21_11_13_-11_-13_21' '21_21_11_-11' '21_21_25_23_21' '21_21_25_25_21'
##########################################################################################################################################
do
 mkdir -p 'sm_'$i'_LI'
 cd 'sm_'$i'_LI'
 echo 'sm_'$i'_LI'
 Sherpa -e0 -g -f /work1/Stephan/OpenLoopsRecola/create_phasespace_points/Run_$i.dat LOOPGEN:=OpenLoops > 'sm_'$i'.txt'
 if [ -f ./makelibs ]; then #needed for gluon final state without using Enable_MHV=0 (since no libraries created)
  ./makelibs
  Sherpa -e0 -g -f /work1/Stephan/OpenLoopsRecola/create_phasespace_points/Run_$i.dat LOOPGEN:=OpenLoops > 'sm_'$i'.txt'
 fi
 #~/RecolaInterface/bin/Sherpa -e0 -g -f ~/OpenLoopsRecola/create_phasespace_points/Run_$i.dat LOOPGEN:=Recola SET_NLO_QCD_PART:=V > 'sm_'$i'_R.txt' #for manual comparison
 python ../format_files_loop-induced.py 'sm_'$i'.txt' 'set_of_points.dat' #create benchmark files with placeholder results
 echo
 echo 'Process '$i' finished'
 echo
 echo
 cd ..
done
