import pkg_resources
from string import Template

sh_template  = Template(pkg_resources.resource_string(__name__, "sh_template.py"))
sh_template_benchmark  = Template(pkg_resources.resource_string(__name__, "sh_template_benchmark.py"))
run_template = Template(pkg_resources.resource_string(__name__, "run_template.dat"))
