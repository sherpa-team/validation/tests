Created file for born tests
Thursday, 11. February 2016 11:14AM

--------------------------------------------------------
Begin test for sm_1_-1_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.092e-12
Minimum deviation                             -4.081e-12
RMS deviation                                  1.290e-12
Average abs. deviation                         9.703e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_21_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 21 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.383e-12
Minimum deviation                             -4.735e-12
RMS deviation                                  1.286e-12
Average abs. deviation                         9.331e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_21_21_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 21 21 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.355e-12
Minimum deviation                             -3.732e-12
RMS deviation                                  1.401e-12
Average abs. deviation                         1.002e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_21_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.434e-12
Minimum deviation                             -4.410e-12
RMS deviation                                  1.760e-12
Average abs. deviation                         1.349e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_2_-2_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 2 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.092e-12
Minimum deviation                             -4.081e-12
RMS deviation                                  1.290e-12
Average abs. deviation                         9.703e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_21_21_2_-2_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 21 21 2 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.355e-12
Minimum deviation                             -3.732e-12
RMS deviation                                  1.401e-12
Average abs. deviation                         1.002e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_24_-24_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 24 -24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.197e-12
Minimum deviation                             -4.277e-12
RMS deviation                                  1.385e-12
Average abs. deviation                         9.851e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_23_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 23 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.693e-12
Minimum deviation                             -4.273e-12
RMS deviation                                  1.251e-12
Average abs. deviation                         9.484e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.700e-12
Minimum deviation                             -3.894e-12
RMS deviation                                  1.255e-12
Average abs. deviation                         9.324e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_22_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.929e-12
Minimum deviation                             -4.149e-12
RMS deviation                                  1.654e-12
Average abs. deviation                         1.268e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_3_-3_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 3 -3 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              1.079e-12
Minimum deviation                             -1.094e-12
RMS deviation                                  5.201e-13
Average abs. deviation                         4.409e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.039e-12
Minimum deviation                             -4.741e-12
RMS deviation                                  1.382e-12
Average abs. deviation                         9.888e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.403e-12
Minimum deviation                             -4.368e-12
RMS deviation                                  1.464e-12
Average abs. deviation                         1.063e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_21_11_-11_1_B
Deviations larger 1e-08:
...none detected
Test for 1 21 -> 11 -11 1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.259e-12
Minimum deviation                             -3.946e-12
RMS deviation                                  1.300e-12
Average abs. deviation                         9.904e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_-2_21_11_-11_-2_B
Deviations larger 1e-08:
...none detected
Test for -2 21 -> 11 -11 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.763e-12
Minimum deviation                             -4.063e-12
RMS deviation                                  1.306e-12
Average abs. deviation                         9.613e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.836e-12
Minimum deviation                             -4.516e-12
RMS deviation                                  1.269e-12
Average abs. deviation                         8.735e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_21_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.091e-12
Minimum deviation                             -3.854e-12
RMS deviation                                  1.270e-12
Average abs. deviation                         8.983e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_21_11_-11_21_1_B
Deviations larger 1e-08:
...none detected
Test for 1 21 -> 11 -11 21 1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.693e-12
Minimum deviation                             -4.328e-12
RMS deviation                                  1.362e-12
Average abs. deviation                         9.731e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_21_21_11_-11_2_-2_B
Deviations larger 1e-08:
...none detected
Test for 21 21 -> 11 -11 2 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.682e-12
Minimum deviation                             -4.290e-12
RMS deviation                                  1.329e-12
Average abs. deviation                         9.454e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_13_-11_-13_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 13 -11 -13 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.278e-12
Minimum deviation                             -4.215e-12
RMS deviation                                  1.319e-12
Average abs. deviation                         1.005e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.133e-12
Minimum deviation                             -4.862e-12
RMS deviation                                  1.540e-12
Average abs. deviation                         1.149e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_13_-14_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> 13 -14 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.906e-12
Minimum deviation                             -4.590e-12
RMS deviation                                  1.443e-12
Average abs. deviation                         1.039e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_13_-14_21_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> 13 -14 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.907e-12
Minimum deviation                             -4.725e-12
RMS deviation                                  1.249e-12
Average abs. deviation                         9.208e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_13_-14_21_21_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> 13 -14 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.787e-12
Minimum deviation                             -4.134e-12
RMS deviation                                  1.363e-12
Average abs. deviation                         1.016e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.890e-12
Minimum deviation                             -4.824e-12
RMS deviation                                  1.450e-12
Average abs. deviation                         1.061e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_3_-3_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 3 -3 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.516e-12
Minimum deviation                             -4.463e-12
RMS deviation                                  1.363e-12
Average abs. deviation                         9.912e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.563e-12
Minimum deviation                             -4.300e-12
RMS deviation                                  1.253e-12
Average abs. deviation                         9.030e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_22_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 22 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.903e-12
Minimum deviation                             -3.693e-12
RMS deviation                                  1.262e-12
Average abs. deviation                         9.337e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_23_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 23 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.244e-12
Minimum deviation                             -4.640e-12
RMS deviation                                  1.294e-12
Average abs. deviation                         9.931e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_23_24_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 23 24 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.115e-12
Minimum deviation                             -3.614e-12
RMS deviation                                  1.346e-12
Average abs. deviation                         9.635e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_22_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 22 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.116e-12
Minimum deviation                             -4.221e-12
RMS deviation                                  1.258e-12
Average abs. deviation                         9.220e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_24_-24_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 24 -24 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.162e-12
Minimum deviation                             -4.081e-12
RMS deviation                                  1.440e-12
Average abs. deviation                         1.040e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_23_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 23 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.489e-12
Minimum deviation                             -4.549e-12
RMS deviation                                  1.371e-12
Average abs. deviation                         1.013e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_22_22_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 22 22 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.192e-12
Minimum deviation                             -4.586e-12
RMS deviation                                  1.273e-12
Average abs. deviation                         9.091e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_22_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 22 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.188e-12
Minimum deviation                             -3.903e-12
RMS deviation                                  1.285e-12
Average abs. deviation                         9.279e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.157e-12
Minimum deviation                             -4.495e-12
RMS deviation                                  1.442e-12
Average abs. deviation                         1.050e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_21_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.913e-12
Minimum deviation                             -4.382e-12
RMS deviation                                  1.249e-12
Average abs. deviation                         9.128e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_-24_22_22_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> -24 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.565e-12
Minimum deviation                             -4.455e-12
RMS deviation                                  1.392e-12
Average abs. deviation                         1.040e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_22_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.218e-12
Minimum deviation                             -4.358e-12
RMS deviation                                  1.353e-12
Average abs. deviation                         9.587e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-2_23_-24_22_B
Deviations larger 1e-08:
...none detected
Test for 1 -2 -> 23 -24 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.094e-12
Minimum deviation                             -4.002e-12
RMS deviation                                  1.425e-12
Average abs. deviation                         1.049e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-3_24_-24_22_B
Deviations larger 1e-08:
...none detected
Test for 3 -3 -> 24 -24 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.457e-12
Minimum deviation                             -3.961e-12
RMS deviation                                  1.405e-12
Average abs. deviation                         9.880e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_24_-24_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 24 -24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.474e-12
Minimum deviation                             -4.583e-12
RMS deviation                                  1.407e-12
Average abs. deviation                         9.641e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_23_23_24_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 23 23 24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.779e-12
Minimum deviation                             -3.378e-12
RMS deviation                                  1.322e-12
Average abs. deviation                         9.574e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_23_23_22_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 23 23 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.571e-12
Minimum deviation                             -4.555e-12
RMS deviation                                  1.418e-12
Average abs. deviation                         1.055e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_22_22_22_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 22 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.136e-12
Minimum deviation                             -4.138e-12
RMS deviation                                  1.389e-12
Average abs. deviation                         9.781e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_24_-24_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 24 -24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.340e-12
Minimum deviation                             -3.889e-12
RMS deviation                                  1.134e-12
Average abs. deviation                         8.509e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_23_23_23_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 23 23 23 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.685e-12
Minimum deviation                             -4.093e-12
RMS deviation                                  1.368e-12
Average abs. deviation                         9.811e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_25_23_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 25 23 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.871e-12
Minimum deviation                             -4.719e-12
RMS deviation                                  1.516e-12
Average abs. deviation                         1.075e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_25_23_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 25 23 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.601e-12
Minimum deviation                             -3.260e-12
RMS deviation                                  1.324e-12
Average abs. deviation                         9.458e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_4_-3_25_24_B
Deviations larger 1e-08:
...none detected
Test for 4 -3 -> 25 24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.107e-12
Minimum deviation                             -3.880e-12
RMS deviation                                  1.526e-12
Average abs. deviation                         1.221e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_25_24_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 25 24 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.807e-12
Minimum deviation                             -3.485e-12
RMS deviation                                  1.238e-12
Average abs. deviation                         8.867e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_25_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 25 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.996e-12
Minimum deviation                             -3.302e-12
RMS deviation                                  1.389e-12
Average abs. deviation                         1.066e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_25_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 25 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.587e-12
Minimum deviation                             -4.124e-12
RMS deviation                                  1.354e-12
Average abs. deviation                         9.993e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_23_24_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 23 24 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.457e-12
Minimum deviation                             -4.535e-12
RMS deviation                                  1.339e-12
Average abs. deviation                         1.013e-12
Ratio failed                                   0.00
--------------------------------------------------------
