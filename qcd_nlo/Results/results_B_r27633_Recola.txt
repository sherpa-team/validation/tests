Created file for born tests
Wednesday, 24. February 2016 05:17PM

--------------------------------------------------------
Begin test for sm_1_-1_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.093e-12
Minimum deviation                             -5.308e-12
RMS deviation                                  1.391e-12
Average abs. deviation                         1.035e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_21_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 21 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.394e-12
Minimum deviation                             -4.729e-12
RMS deviation                                  1.300e-12
Average abs. deviation                         9.520e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_21_21_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 21 21 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.431e-12
Minimum deviation                             -1.213e-11
RMS deviation                                  1.671e-12
Average abs. deviation                         1.084e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_21_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.434e-12
Minimum deviation                             -4.406e-12
RMS deviation                                  1.760e-12
Average abs. deviation                         1.350e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_2_-2_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 2 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.093e-12
Minimum deviation                             -5.308e-12
RMS deviation                                  1.391e-12
Average abs. deviation                         1.035e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_21_21_2_-2_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 21 21 2 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.431e-12
Minimum deviation                             -1.213e-11
RMS deviation                                  1.671e-12
Average abs. deviation                         1.084e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_24_-24_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 24 -24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.442e-12
Minimum deviation                             -4.207e-12
RMS deviation                                  1.341e-12
Average abs. deviation                         9.632e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_23_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 23 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.672e-12
Minimum deviation                             -4.274e-12
RMS deviation                                  1.252e-12
Average abs. deviation                         9.475e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.686e-12
Minimum deviation                             -3.958e-12
RMS deviation                                  1.265e-12
Average abs. deviation                         9.429e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_22_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.917e-12
Minimum deviation                             -4.022e-12
RMS deviation                                  1.615e-12
Average abs. deviation                         1.239e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_3_-3_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 3 -3 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.649e-12
Minimum deviation                             -1.313e-11
RMS deviation                                  1.179e-12
Average abs. deviation                         5.791e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.195e-12
Minimum deviation                             -4.739e-12
RMS deviation                                  1.396e-12
Average abs. deviation                         1.005e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.401e-12
Minimum deviation                             -6.120e-12
RMS deviation                                  1.549e-12
Average abs. deviation                         1.116e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_21_11_-11_1_B
Deviations larger 1e-08:
...none detected
Test for 1 21 -> 11 -11 1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.245e-12
Minimum deviation                             -3.847e-12
RMS deviation                                  1.317e-12
Average abs. deviation                         1.016e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_-2_21_11_-11_-2_B
Deviations larger 1e-08:
...none detected
Test for -2 21 -> 11 -11 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              1.097e-11
Minimum deviation                             -4.938e-12
RMS deviation                                  1.565e-12
Average abs. deviation                         1.049e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.084e-12
Minimum deviation                             -7.674e-12
RMS deviation                                  1.499e-12
Average abs. deviation                         9.787e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_21_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.217e-12
Minimum deviation                             -3.829e-12
RMS deviation                                  1.238e-12
Average abs. deviation                         8.875e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_21_11_-11_21_1_B
Deviations larger 1e-08:
...none detected
Test for 1 21 -> 11 -11 21 1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.685e-12
Minimum deviation                             -9.086e-12
RMS deviation                                  1.503e-12
Average abs. deviation                         1.027e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_21_21_11_-11_2_-2_B
Deviations larger 1e-08:
...none detected
Test for 21 21 -> 11 -11 2 -2 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              5.134e-12
Minimum deviation                             -4.487e-12
RMS deviation                                  1.379e-12
Average abs. deviation                         9.785e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_13_-11_-13_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 13 -11 -13 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.477e-11
Minimum deviation                             -4.217e-12
RMS deviation                                  2.787e-12
Average abs. deviation                         1.161e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.956e-12
Minimum deviation                             -1.149e-11
RMS deviation                                  1.753e-12
Average abs. deviation                         1.221e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_13_-14_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> 13 -14 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.792e-12
Minimum deviation                             -1.420e-11
RMS deviation                                  1.802e-12
Average abs. deviation                         1.144e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_13_-14_21_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> 13 -14 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.740e-12
Minimum deviation                             -4.728e-12
RMS deviation                                  1.290e-12
Average abs. deviation                         9.732e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_13_-14_21_21_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> 13 -14 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.797e-12
Minimum deviation                             -3.613e-11
RMS deviation                                  2.915e-12
Average abs. deviation                         1.253e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.765e-12
Minimum deviation                             -4.909e-12
RMS deviation                                  1.572e-12
Average abs. deviation                         1.159e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_3_-3_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 3 -3 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              9.422e-11
Minimum deviation                             -4.577e-12
RMS deviation                                  6.832e-12
Average abs. deviation                         1.582e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.561e-12
Minimum deviation                             -4.265e-12
RMS deviation                                  1.266e-12
Average abs. deviation                         9.153e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_22_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 22 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.909e-12
Minimum deviation                             -3.694e-12
RMS deviation                                  1.270e-12
Average abs. deviation                         9.407e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_23_21_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 23 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.243e-12
Minimum deviation                             -4.590e-12
RMS deviation                                  1.296e-12
Average abs. deviation                         9.952e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_23_24_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 23 24 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.121e-12
Minimum deviation                             -3.619e-12
RMS deviation                                  1.340e-12
Average abs. deviation                         9.623e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_22_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 22 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.118e-12
Minimum deviation                             -1.937e-11
RMS deviation                                  1.865e-12
Average abs. deviation                         1.022e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_24_-24_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 24 -24 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.161e-12
Minimum deviation                             -2.141e-11
RMS deviation                                  2.246e-12
Average abs. deviation                         1.201e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_23_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 23 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.399e-12
Minimum deviation                             -4.599e-12
RMS deviation                                  1.371e-12
Average abs. deviation                         1.012e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_22_22_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 22 22 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.258e-12
Minimum deviation                             -4.587e-12
RMS deviation                                  1.272e-12
Average abs. deviation                         9.140e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_22_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 22 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.156e-12
Minimum deviation                             -3.905e-12
RMS deviation                                  1.287e-12
Average abs. deviation                         9.316e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.155e-12
Minimum deviation                             -4.521e-12
RMS deviation                                  1.447e-12
Average abs. deviation                         1.053e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_21_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 21 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.905e-12
Minimum deviation                             -4.372e-12
RMS deviation                                  1.266e-12
Average abs. deviation                         9.246e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-4_-24_22_22_B
Deviations larger 1e-08:
...none detected
Test for 3 -4 -> -24 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.423e-12
Minimum deviation                             -4.464e-12
RMS deviation                                  1.378e-12
Average abs. deviation                         1.047e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_22_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.177e-12
Minimum deviation                             -4.292e-12
RMS deviation                                  1.351e-12
Average abs. deviation                         9.594e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-2_23_-24_22_B
Deviations larger 1e-08:
...none detected
Test for 1 -2 -> 23 -24 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.803e-12
Minimum deviation                             -4.020e-12
RMS deviation                                  1.366e-12
Average abs. deviation                         9.968e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_3_-3_24_-24_22_B
Deviations larger 1e-08:
...none detected
Test for 3 -3 -> 24 -24 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.503e-12
Minimum deviation                             -3.673e-12
RMS deviation                                  1.375e-12
Average abs. deviation                         9.964e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_23_24_-24_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 23 24 -24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.473e-12
Minimum deviation                             -3.777e-12
RMS deviation                                  1.478e-12
Average abs. deviation                         1.060e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_23_23_24_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 23 23 24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.580e-12
Minimum deviation                             -4.407e-12
RMS deviation                                  1.411e-12
Average abs. deviation                         1.057e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_23_23_22_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 23 23 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.569e-12
Minimum deviation                             -4.555e-12
RMS deviation                                  1.413e-12
Average abs. deviation                         1.052e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_22_22_22_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 22 22 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.130e-12
Minimum deviation                             -4.139e-12
RMS deviation                                  1.398e-12
Average abs. deviation                         9.900e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_24_24_-24_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 24 24 -24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.963e-12
Minimum deviation                             -3.147e-12
RMS deviation                                  1.221e-12
Average abs. deviation                         8.714e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_23_23_23_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 23 23 23 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.089e-12
Minimum deviation                             -4.339e-12
RMS deviation                                  1.408e-12
Average abs. deviation                         9.797e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_25_23_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 25 23 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.740e-12
Minimum deviation                             -4.129e-12
RMS deviation                                  1.402e-12
Average abs. deviation                         9.636e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-2_25_23_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -2 -> 25 23 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.620e-12
Minimum deviation                             -2.484e-11
RMS deviation                                  2.233e-12
Average abs. deviation                         1.090e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_4_-3_25_24_B
Deviations larger 1e-08:
...none detected
Test for 4 -3 -> 25 24 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.848e-12
Minimum deviation                             -3.984e-12
RMS deviation                                  1.480e-12
Average abs. deviation                         1.165e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_25_24_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 25 24 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.811e-12
Minimum deviation                             -1.615e-11
RMS deviation                                  1.691e-12
Average abs. deviation                         9.788e-13
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_1_-1_11_-11_25_1_-1_B
Deviations larger 1e-08:
...none detected
Test for 1 -1 -> 11 -11 25 1 -1 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              3.992e-12
Minimum deviation                             -1.080e-11
RMS deviation                                  1.654e-12
Average abs. deviation                         1.171e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_-11_12_25_21_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> -11 12 25 21 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              9.792e-12
Minimum deviation                             -1.176e-11
RMS deviation                                  1.767e-12
Average abs. deviation                         1.148e-12
Ratio failed                                   0.00
--------------------------------------------------------

--------------------------------------------------------
Begin test for sm_2_-1_23_24_22_B
Deviations larger 1e-08:
...none detected
Test for 2 -1 -> 23 24 22 passed (ratio < 0.05): 
Expected deviation                             1.000e-08
Maximum deviation                              4.676e-12
Minimum deviation                             -3.987e-12
RMS deviation                                  1.259e-12
Average abs. deviation                         9.480e-13
Ratio failed                                   0.00
--------------------------------------------------------
