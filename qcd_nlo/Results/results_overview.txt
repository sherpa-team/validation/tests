Created overview file for virtual and born tests
Friday, 08. April 2016 12:11PM
The test is passed if the ratio of phase space points larger then the threshold is less then 0.05
Test for sm_1_-1_1_-1_V                      passed
Test for sm_1_-1_21_1_-1_V                   passed
Test for sm_1_-1_21_21_1_-1_V                not passed
Test for sm_2_-2_21_21_V                     passed
Test for sm_2_-2_2_-2_V                      passed
Test for sm_2_-2_21_21_2_-2_V                not passed
Test for sm_2_-2_24_-24_V                    passed
Test for sm_2_-2_23_23_V                     passed
Test for sm_2_-2_23_22_V                     passed
Test for sm_2_-2_22_22_V                     passed
Test for sm_2_-2_3_-3_V                      passed
Test for sm_1_-1_11_-11_V                    passed
Test for sm_1_-1_11_-11_21_V                 passed
Test for sm_1_21_11_-11_1_V                  passed
Test for sm_-2_21_11_-11_-2_V                passed
Test for sm_1_-1_11_-11_1_-1_V               passed
Test for sm_1_-1_11_-11_21_21_V              passed
Test for sm_1_21_11_-11_21_1_V               passed
Test for sm_21_21_11_-11_2_-2_V              passed
Test for sm_1_-1_11_13_-11_-13_V             passed
Test for sm_2_-1_-11_12_V                    passed
Test for sm_3_-4_13_-14_V                    passed
Test for sm_3_-4_13_-14_21_V                 passed
Test for sm_3_-4_13_-14_21_21_V              passed
Test for sm_2_-1_-11_12_3_-3_V               passed
Test for sm_2_-1_24_21_V                     passed
Test for sm_1_-1_22_21_V                     passed
Test for sm_1_-1_23_21_V                     passed
Test for sm_2_-1_23_24_21_V                  passed
Test for sm_2_-1_24_22_21_V                  passed
Test for sm_2_-2_24_-24_21_V                 not passed
Test for sm_2_-2_23_23_21_V                  not passed
Test for sm_2_-2_22_22_21_V                  not passed
Test for sm_1_-1_22_1_-1_V                   passed
Test for sm_2_-2_23_1_-1_V                   passed
Test for sm_2_-1_24_21_21_V                  passed
Test for sm_3_-4_-24_22_22_V                 passed
Test for sm_2_-2_23_22_22_V                  passed
Test for sm_1_-2_23_-24_22_V                 passed
Test for sm_3_-3_24_-24_22_V                 passed
Test for sm_2_-2_23_24_-24_V                 passed
Test for sm_2_-1_23_23_24_V                  passed
Test for sm_1_-1_23_23_22_V                  passed
Test for sm_1_-1_22_22_22_V                  passed
Test for sm_2_-1_24_24_-24_V                 passed
Test for sm_1_-1_23_23_23_V                  passed
Test for sm_2_-2_25_23_V                     passed
Test for sm_2_-2_25_23_21_V                  passed
Test for sm_4_-3_25_24_V                     passed
Test for sm_2_-1_25_24_1_-1_V                passed
Test for sm_1_-1_11_-11_25_1_-1_V            passed
Test for sm_2_-1_-11_12_25_21_V              passed
Test for sm_2_-1_23_24_22_V                  passed